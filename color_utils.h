#ifndef COLORUTILS_H
#define COLORUTILS_H

#include "FastLed.h"

class ColorUtils
{


  static void PrintColor(CRGB c);

  static void PrintColor(CHSV c);


  static void CheckColor(float r, float g, float b);
    
  static CRGB randomcolor();

  static CHSV randomhsv();
    
  static void RGBtoHSV(float& fR, float& fG, float& fB, float& fH, float& fS, float& fV);

  static CHSV rgb2hsv(CRGB &in);

};
  
#endif
