
#include "leds.h"
#include "BurstEffect.h"


	// Start a burst every so often
	Timer burstTimer;

	// NOTE: the order of the strips is backward for left and right halves of a panel!
	int curLoc[NUM_PANEL_STRIPS];
	CRGB burstColor;

	void GetLocation(int strip, int led, float* x, float* y)
	{
		 // Return a float location of the led in a 1.0 by 1.0 space
		*x = (float)(7 - strip)/7.0f;
		*y = (float)(led)/(float)(LEDS_PER_PANEL_STRIP);
	}

	void BurstEffect::Init()
	{
		burstColor = CRGB::Orange;
		burstTimer.SetFrequency(5.0f);
		burstTimer.Start();
	}

	void BurstEffect::Update(unsigned long deltamics)
	{
		burstTimer.Update(deltamics);
		if (burstTimer.GetEvent())
		{
			// Launch a burst
		}
		float wavePhase = (float)(beat16(120))/65536.0f;
		// For now do a trig function on the x,y location using a sine wave to vary the hue
		for (int strip=0; strip<NUM_PANEL_STRIPS; strip++)
		{
			for (int led=0; led<LEDS_PER_PANEL_STRIP; led++)
			{
				float x,y;
				GetLocation(strip,led,&x,&y);
				float sqrdist = (x*x + y*y); 
				uint8_t hue = (int)(sqrdist*255 + wavePhase*255) % 255;
				leds[strip*LEDS_PER_PANEL_STRIP + led] = CHSV(hue,255,128);
			}
		}
	}

	void BurstEffect::Render()
	{

	}
