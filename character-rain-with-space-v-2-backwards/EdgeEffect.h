#ifndef EDGEEFFECT
#define EDGEEFFECT

// The raindrops that we will be managing
#include <arduino.h>
#include "Effect.h"
#include "constants.h"
#include "tunables.h"
#include "timer.h"

// Normal mode is blues and greens, maxmode is red and blue, burstmode is rainbow hues
// maxmode overrides normal mode, and burstmode overrides both
#define NORMALMODE 0
#define MAXMODE 1
#define EDGEBURST 2

// Raineffect owns some "lanes" which correspond to the strands. It spawns raindrops in random lanes. It knows which lanes have active raindrops and always chooses an empty lane in which to spawn
class EdgeEffect : Effect
{


public:
	
	
	Timer edgeShiftTimer;
	Timer hueShiftTimer;

	uint8_t mode;

	CRGB patternBuffer[32];
	int patternSize = 32;
	int patternCounter = 0;

// Some hues to vary around
	uint8_t hueA, hueB;
	uint8_t satA, satB;
	uint8_t valA, valB;
	
	int hueIncrementA, hueIncrementB;

	void ShiftOut(int edgeNum, int lineNum);
	void SetMiddle(int edgeNum, int lineNum, CRGB color);
	void SetRainbowPattern();

	void Init();

	void Update(unsigned long deltamics);

	void Render();
};


#endif