#ifndef STRIPTEST
#define STRIPTEST

// The raindrops that we will be managing
#include <arduino.h>
#include "Effect.h"
#include "constants.h"
#include "tunables.h"
#include "timer.h"

// Raineffect owns some "lanes" which correspond to the strands. It spawns raindrops in random lanes. It knows which lanes have active raindrops and always chooses an empty lane in which to spawn
class StripTest : Effect
{
	// a readonly set of test colors
	static const CRGB testColors[8];
	
	// Start a burst every so often
	Timer testTimer;
	int phase;
	int curColor;


public:
	// <<TODO>> Consider promoting the concept of "done" to the effect class 
	bool done;

	void Init();

	void Update(unsigned long deltamics);

	void Render(); 

	void SetColorByColumn();
	void SetColorByIndex();
};


#endif