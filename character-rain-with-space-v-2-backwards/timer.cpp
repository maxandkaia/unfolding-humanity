#include "timer.h"

 
  void Timer::Reset()
  {
    micstilevent = micsperevent;
    eventTriggered = false;
  }

  void Timer::SetMode(TimerMode m)
  {
    mode = m;
  }

  void Timer::Start()
  {
    running = true;
  }

  void Timer::Stop()
  {
    running = false;
  }

  void Timer::SetFrequency(float f)
  {
    // How many mics to wait til next event
    unsigned long elapsed = micsperevent - micstilevent;
    micsperevent = (unsigned long)((1.0f/f) * 1000000.0f);
    micstilevent = micsperevent;
    if (micsperevent > elapsed)
    {
      micstilevent = micsperevent - elapsed;
    }
    else
      micstilevent = micsperevent;
  }

  void Timer::SetInterval(float seconds)
  {
    unsigned long elapsed = micsperevent - micstilevent;
    micsperevent = (unsigned long)(seconds * 1000000.0f);
    micstilevent = micsperevent;
    if (micsperevent > elapsed)
    {
      micstilevent = micsperevent - elapsed;
    }
    else
      micstilevent = micsperevent;
  }

  void Timer::SetInterval(unsigned long milliseconds)
  {
    unsigned long elapsed = micsperevent - micstilevent;
    micsperevent = milliseconds*1000;
    micstilevent = micsperevent;
    if (micsperevent > elapsed)
    {
      micstilevent = micsperevent - elapsed;
    }
    else
      micstilevent = micsperevent;
  }

  void Timer::Update(unsigned long mics)
  {
    if (!running) return;
    micstilevent -= mics;
    if (micstilevent <= 0)
    {
      micstilevent += micsperevent;
      eventTriggered = true;
      
      if (mode == STOP)
      {
        micstilevent = micsperevent;
        running = false;
      }
    }
  }

  float Timer::GetSecondsRemaining()
  {
    // Convert micstilevent into seconds and return it
    return (float)(micstilevent)/1000000.0f;
  }

 bool Timer::GetEvent()
 {
  bool retVal = eventTriggered;
  if (retVal)
    eventTriggered = false;
  return retVal;
 }

