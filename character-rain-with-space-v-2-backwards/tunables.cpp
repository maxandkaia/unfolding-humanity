
#include <arduino.h>
#include "FastLED.h"
#include "tunables.h"
#include <EEPROM.h>


// Color of chars to drop
CRGB TunablesManager::dropcolor;
CRGB TunablesManager::trailcolor;

uint8_t TunablesManager::rainfademin;
uint8_t TunablesManager::rainfademax;

// Milliseconds to wait before spawning a new drop, chosen randomly each new drop
uint TunablesManager::minspawnwait;
uint TunablesManager::maxspawnwait;

// Drop speed range in chars per second
float TunablesManager::mindropspeed;
float TunablesManager::maxdropspeed;

float TunablesManager::edgespeed; // in pixels per second (max 60 due to framerate limit)
int TunablesManager::edgedirection; // 0 = inside out, 1 = outside in, 2 = round the pentagon forwards, 3 = round the pentagon backwards
int TunablesManager::edgecolor; // 0 = red and yellow, 1 = hue shifting, 2 = 

int TunablesManager::edgemode; // 0 = stripes crawler, 1 = sparkles
int TunablesManager::starmode; 

float TunablesManager::starfreq;
int TunablesManager::stardensity;

float TunablesManager::minstarfadein;
float TunablesManager::maxstarfadein;
float TunablesManager::minstarfadeout;
float TunablesManager::maxstarfadeout;

CHSV TunablesManager::whitestarcolors[5];
//CHSV TunablesManager::starcolors[5];

int TunablesManager::minstarlifetime;
int TunablesManager::maxstarlifetime;

int TunablesManager::glitterdensity;
float TunablesManager::glitterfreq;

int TunablesManager::cometdensity;
float TunablesManager::cometfreq;

void PrintValue(const char* name, int value)
{
	Serial.print(name); Serial.print(" = "); Serial.println(value);
}

void PrintValue(const char* name, uint value)
{
	Serial.print(name); Serial.print(" = "); Serial.println(value);
}

void PrintValue(const char* name, float value)
{
	Serial.print(name); Serial.print(" = "); Serial.println(value);
}

void PrintValue(const char* name, CRGB value)
{
	Serial.print(name); Serial.print(" = ");
	Serial.print(value.red); Serial.print(",");
	Serial.print(value.green); Serial.print(",");
	Serial.print(value.blue);
	Serial.println();
}

int WriteTunable(int address, uint val)
{
	unsigned char* ptr = (unsigned char*) &val;
	for (uint i=0; i<sizeof(uint); i++)
		EEPROM.write(address++, *ptr++);
	return address;
}

int WriteTunable(int address, uint8_t val)
{
	unsigned char* ptr = (unsigned char*) &val;
	for (uint i=0; i<sizeof(uint8_t); i++)
		EEPROM.write(address++, *ptr++);
	return address;
}

int WriteTunable(int address, float val)
{
	unsigned char* ptr = (unsigned char*) &val;
	for (uint i=0; i<sizeof(float); i++)
		EEPROM.write(address++, *ptr++);
	return address;
}

int WriteTunable(int address, int val)
{
	unsigned char* ptr = (unsigned char*) &val;
	for (uint i=0; i<sizeof(int); i++)
		EEPROM.write(address++, *ptr++);
	return address;
}

int WriteTunable(int address, CRGB val)
{
	unsigned char* ptr = (unsigned char*) &val;
	for (uint i=0; i<sizeof(CRGB); i++)
		EEPROM.write(address++, *ptr++);
	return address;
}

int WriteTunable(int address, CHSV val)
{
	unsigned char* ptr = (unsigned char*) &val;
	for (uint i=0; i<sizeof(CHSV); i++)
		EEPROM.write(address++, *ptr++);
	return address;
}


int ReadTunable(int address, uint* val)
{
	unsigned char* ptr = (unsigned char*) val;
	for (uint i=0; i<sizeof(uint); i++)
		*ptr++ = EEPROM.read(address++);
	return address;
}

int ReadTunable(int address, uint8_t* val)
{
	unsigned char* ptr = (unsigned char*) val;
	for (uint i=0; i<sizeof(uint8_t); i++)
		*ptr++ = EEPROM.read(address++);
	return address;
}

int ReadTunable(int address, float* val)
{
	unsigned char* ptr = (unsigned char*) val;
	for (uint i=0; i<sizeof(float); i++)
		*ptr++ = EEPROM.read(address++);
	return address;
}

int ReadTunable(int address, int* val)
{
	unsigned char* ptr = (unsigned char*) val;
	for (uint i=0; i<sizeof(int); i++)
		*ptr++ = EEPROM.read(address++);
	return address;
}

int ReadTunable(int address, CRGB* val)
{
	unsigned char* ptr = (unsigned char*) val;
	for (uint i=0; i<sizeof(CRGB); i++)
		*ptr++ = EEPROM.read(address++);
	return address;
}

int ReadTunable(int address, CHSV* val)
{
	unsigned char* ptr = (unsigned char*) val;
	for (uint i=0; i<sizeof(CHSV); i++)
		*ptr++ = EEPROM.read(address++);
	return address;
}


void TunablesManager::WriteTunablesToEeprom()
{
	ClearEeprom();
	int address = 0;
	address = WriteTunable(address, dropcolor);
	address = WriteTunable(address, trailcolor);


	address = WriteTunable(address, rainfademin);
	address = WriteTunable(address, rainfademax);


	address = WriteTunable(address, minspawnwait);
	address = WriteTunable(address, maxspawnwait);
	address = WriteTunable(address, mindropspeed);
	address = WriteTunable(address, maxdropspeed);

	address = WriteTunable(address, edgespeed);
	address = WriteTunable(address, edgedirection);
	address = WriteTunable(address, edgecolor);
	address = WriteTunable(address, edgemode);

	address = WriteTunable(address, starmode);
	address = WriteTunable(address, starfreq);
	address = WriteTunable(address, stardensity);
	address = WriteTunable(address, minstarfadein);
	address = WriteTunable(address, minstarfadeout);
	address = WriteTunable(address, maxstarfadein);
	address = WriteTunable(address, maxstarfadeout);

	address = WriteTunable(address, whitestarcolors[0]);
	address = WriteTunable(address, whitestarcolors[1]);
	address = WriteTunable(address, whitestarcolors[2]);
	address = WriteTunable(address, whitestarcolors[3]);
	address = WriteTunable(address, whitestarcolors[4]);

	address = WriteTunable(address, minstarlifetime);
	address = WriteTunable(address, maxstarlifetime);

	address = WriteTunable(address, glitterdensity);
	address = WriteTunable(address, glitterfreq);

	address = WriteTunable(address, cometdensity);
	address = WriteTunable(address, cometfreq);
}

void TunablesManager::ReadTunablesFromEeprom()
{
	int address = 0;
	address = ReadTunable(address, &dropcolor);
	address = ReadTunable(address, &trailcolor);

	address = ReadTunable(address, &rainfademin);
	address = ReadTunable(address, &rainfademax);

	address = ReadTunable(address, &minspawnwait);
	address = ReadTunable(address, &maxspawnwait);
	address = ReadTunable(address, &mindropspeed);
	address = ReadTunable(address, &maxdropspeed);


	address = ReadTunable(address, &edgespeed);
	address = ReadTunable(address, &edgedirection);
	address = ReadTunable(address, &edgecolor);
	address = ReadTunable(address, &edgemode);

	address = ReadTunable(address, &starmode);
	address = ReadTunable(address, &starfreq);
	address = ReadTunable(address, &stardensity);
	address = ReadTunable(address, &minstarfadein);
	address = ReadTunable(address, &minstarfadeout);
	address = ReadTunable(address, &maxstarfadein);
	address = ReadTunable(address, &maxstarfadeout);

	address = ReadTunable(address, &whitestarcolors[0]);
	address = ReadTunable(address, &whitestarcolors[1]);
	address = ReadTunable(address, &whitestarcolors[2]);
	address = ReadTunable(address, &whitestarcolors[3]);
	address = ReadTunable(address, &whitestarcolors[4]);

	address = ReadTunable(address, &minstarlifetime);
	address = ReadTunable(address, &maxstarlifetime);

	address = ReadTunable(address, &glitterdensity);
	address = ReadTunable(address, &glitterfreq);

	address = ReadTunable(address, &cometdensity);
	address = ReadTunable(address, &cometfreq);
}

void TunablesManager::ClearEeprom()
{
  for (int i = 0 ; i < EEPROM.length() ; i++) {
    EEPROM.write(i, 0);
  }
}

void TunablesManager::PrintTunables()
{
	PrintValue("dropcolor", dropcolor);
	PrintValue("trailcolor", trailcolor);

	PrintValue("rainfademin", rainfademin);
	PrintValue("rainfademax", rainfademax);

	PrintValue("minspawnwait", minspawnwait);
	PrintValue("maxspawnwait", maxspawnwait);
	PrintValue("mindropspeed", mindropspeed);
	PrintValue("maxdropspeed", maxdropspeed);
	
	PrintValue("edgespeed", edgespeed);
	PrintValue("edgedirection", edgedirection);
	PrintValue("edgecolor", edgecolor);
	PrintValue("edgemode", edgemode);
	
	PrintValue("starmode", starmode);
	PrintValue("starfreq", starfreq);
	PrintValue("stardensity", stardensity);
	PrintValue("minstarfadein", minstarfadein);
	PrintValue("maxstarfadein", maxstarfadein);
	PrintValue("minstarfadein", minstarfadein);
	PrintValue("minstarfadein", minstarfadein);

	PrintValue("whitestarcolor", whitestarcolors[0]);
	PrintValue("whitestarcolor", whitestarcolors[1]);
	PrintValue("whitestarcolor", whitestarcolors[2]);
	PrintValue("whitestarcolor", whitestarcolors[3]);
	PrintValue("whitestarcolor", whitestarcolors[4]);

	PrintValue("minstarlifetime", minstarlifetime);
	PrintValue("maxstarlifetime", maxstarlifetime);

	PrintValue("glitterdensity", glitterdensity);
	PrintValue("glitterfreq", glitterfreq);

	PrintValue("cometdensity", cometdensity);
	PrintValue("cometfreq", cometfreq);

}

// If we stick with the monolithic project structure (all inline .h files) we can just init these values
// in the declarations above and get rid of this
void TunablesManager::SetTunableDefaults()
{

//dropcolor = CRGB::Green; // default to green
dropcolor = CRGB(90,255,50); // default to green
trailcolor = CRGB(0,128,0); // default to green

rainfademin = 1;
rainfademax = 20;

minspawnwait = 100;
maxspawnwait = 500;

mindropspeed = 8.0f; // 10 chars per second speed by default
maxdropspeed = 9.0f;

edgespeed = 60.0f; // in pixels per second (max 60 due to framerate limit)
edgedirection = 0; // 0 = inside out, 1 = outside in, 2 = round the pentagon forwards, 3 = round the pentagon backwards
edgecolor = 0; // 0 = red and yellow, 1 = hue shifting, 2 = 

edgemode = 0; // 0 = stripes crawler, 1 = sparkles
starmode = 7;

stardensity = 40;
starfreq = 30.0f;
minstarfadein = 0.1f;
maxstarfadein = 2.0f;
minstarfadeout = 0.1f;
maxstarfadeout = 10.0f;

whitestarcolors[0] = CHSV(0,35,255);
whitestarcolors[1] = CHSV(250,35,255);
whitestarcolors[2] = CHSV(230,35,255);
whitestarcolors[3] = CHSV(220,35,255);
whitestarcolors[4] = CHSV(200,35,255);

minstarlifetime = 1000000;
maxstarlifetime = 10000000;

glitterdensity = 10;
glitterfreq = 20.0f;

cometdensity = 3;
cometfreq = 2.0f;
}

void TunablesManager::PrintHelp()
{
		Serial.println("");
		Serial.println("Available functions:");
		Serial.println("");
		Serial.println("dropcolor [r,g,b]					(sets the color of the character rain drops)");
		Serial.println("trailcolor [r,g,b]					(sets the color of the character rain trail)");
		
		Serial.println("rainfademin [fade]				(sets the min raindrop per-frame fade amount)");
		Serial.println("rainfademax [fade]				(sets the max raindrop per-frame fade amount)");

		Serial.println("mindropspeed [speed]				(sets the min raindrop speed in characters per second)");
		Serial.println("maxdropspeed [speed]				(sets the max raindrop speed in characters per second)");
		Serial.println("minspawnwait [milliseconds]			(sets the min wait between drops)");
		Serial.println("maxspawnwait [milliseconds]			(sets the max wait between drops)");
		Serial.println("");

		Serial.println("edgespeed [speed]					(sets the edge crawl speed in pixels per second)");
		Serial.println("edgedirection [direction]			(sets the direction of the edge crawl, [0..3] )");
		Serial.println("edgecolor [mode]					(sets the edge crawl color scheme [0..2])");
		Serial.println("edgemode [mode]						(sets the edge effect mode, 0=crawl, 1=sparkles");
		Serial.println("");

		Serial.println("starmode [mode]						(sets the star effect mode");
		Serial.println("starfreq [freq]	    				(birthrate of stars)");
		Serial.println("stardensity [density]	    		(percentage of LEDs which are stars)");

		Serial.println("minstarfadein [seconds]	    		(minimum fade in time)");
		Serial.println("maxstarfadein [seconds]	    		(maximum fade in time)");
		Serial.println("minstarfadeout [seconds]	    	(minimum fade out time)");
		Serial.println("maxstarfadeout [seconds]	    	(maximum fade out time)");

		Serial.println("whtiestarcolor [colornum] [CHSV color] 	(set the color choices for stars)");

		Serial.println("minstarlifetime [seconds]           (min lifetime for stars)");
		Serial.println("maxstarlifetime [seconds]           (max lifetime for stars)");
		Serial.println("");

		Serial.println("glitterdensity [density]	    	(sets the percentage of stars that are updated in glitter mode)");
		Serial.println("glitterfreq [freq]	    			(number of times per second we refresh the glitterscape)");
		Serial.println("");

		Serial.println("cometdensity [density]	        	(sets the percentage of stars that are updated in glitter mode)");
		Serial.println("cometfreq [freq]	    			(number of times per second we refresh the glitterscape)");
		Serial.println("");
		
		Serial.println("print 								(prints the current tunables values)");
		Serial.println("read 								(reads the current tunables values from EEPROM)");
		Serial.println("write 								(writes the current tunables values to EEPROM)");
		Serial.println("announce							(prints the name/type of the attached controller)");
		Serial.println("");

}

// Returns true if the message was a valid param set
bool TunablesManager::HandleMessage(const char* message)
{
	// Try to parse and see if this is a valid param to set
	char paramtoset[24];
	int ret = sscanf(message,"%23s ", &paramtoset[0]);
	if (ret != 1)
	{
		Serial.println("could not parse tunable set command");
		Serial.print("message was: "); Serial.println(message);
		Serial.print("ret was:"); Serial.println(ret);
		Serial.print("paramtoset was: "); Serial.println(paramtoset);
		return false;
	}

	Serial.print("attempting to set parameter: "); Serial.println(paramtoset);	
	int r,g,b;
	int h,s,v;
	int intval;
	float floatval;

	if (!strcmp(paramtoset,"dropcolor"))
	{

		int ret = sscanf(message,"%23s %d,%d,%d",&paramtoset[0],&r,&g,&b);
		if (ret == 4)
		{
			Serial.println("setting!");
			dropcolor = CRGB(r,g,b);
			return true;
		}
	}
	if (!strcmp(paramtoset,"trailcolor"))
	{

		int ret = sscanf(message,"%23s %d,%d,%d",&paramtoset[0],&r,&g,&b);
		if (ret == 4)
		{
			Serial.println("setting!");
			trailcolor = CRGB(r,g,b);
			return true;
		}
	}
	else if (!strcmp(paramtoset,"rainfademin"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval <=0 || intval > 254) return false;
			Serial.println("Setting!");
			rainfademin = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"rainfademax"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval <=0 || intval > 254) return false;
			Serial.println("Setting!");
			rainfademax = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"mindropspeed"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval >= maxdropspeed) return false;
			Serial.println("Setting!");
			mindropspeed = floatval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"maxdropspeed"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval <= mindropspeed) return false;
			Serial.println("Setting!");
			maxdropspeed = floatval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"minspawnwait"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval >= maxspawnwait) return false;
			Serial.println("Setting!");
			minspawnwait = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"maxspawnwait"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval <= minspawnwait) return false;
			Serial.println("Setting!");
			maxspawnwait = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"edgespeed"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval < 1.0f || floatval > 60.0f) return false;
			Serial.println("Setting!");
			edgespeed = floatval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"edgecolor"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval > 2 || intval < 0) return false;
			Serial.println("Setting!");
			edgecolor = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"edgedirection"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval < 0 || intval > 3) return false;
			Serial.println("Setting!");
			edgedirection = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"edgemode"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval < 0 || intval > 1) return false;
			Serial.println("Setting!");
			edgemode = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"starmode"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval < 0 || intval > 30) return false;
			Serial.println("Setting!");
			starmode = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"starfreq"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval < 0 || floatval > 60) return false;
			Serial.println("Setting!");
			starfreq = floatval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"stardensity"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval < 0 || intval > 100) return false;
			Serial.println("Setting!");
			stardensity = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"minstarfadein"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval < 0 || floatval > maxstarfadein) return false;
			Serial.println("Setting!");
			minstarfadein = floatval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"maxstarfadein"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval < minstarfadein || floatval > 4) return false;
			Serial.println("Setting!");
			maxstarfadein = floatval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"minstarfadeout"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval < 0 || floatval > maxstarfadeout) return false;
			Serial.println("Setting!");
			minstarfadeout = floatval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"maxstarfadeout"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval < minstarfadeout || floatval > 4) return false;
			Serial.println("Setting!");
			maxstarfadeout = floatval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"glitterdensity"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval < 0 || intval > 100) return false;
			Serial.println("Setting!");
			glitterdensity = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"glitterfreq"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval < 0 || floatval > 120) return false;
			Serial.println("Setting!");
			glitterfreq = floatval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"cometdensity"))
	{
		int ret = sscanf(message,"%23s %d",&paramtoset[0],&intval);
		if (ret == 2)
		{
			if (intval < 0 || intval > 100) return false;
			Serial.println("Setting!");
			cometdensity = intval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"cometfreq"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval < 0 || floatval > 120) return false;
			Serial.println("Setting!");
			cometfreq = floatval;
			return true;
		}
	}
	else if (!strcmp(paramtoset,"whitestarcolor"))
	{
		int ret = sscanf(message,"%23s %d %d,%d,%d",&paramtoset[0],&intval, &h, &s, &v);
		if (ret == 6)
		{
			if (intval < 0 || intval > 5) return false;
			Serial.println("Setting!");
			whitestarcolors[intval] = CHSV(h,s,v);
			return true;
		}
	}
	else if (!strcmp(paramtoset,"minstarlifetime"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval < 0 || floatval > 20) return false;
			Serial.println("Setting!");
			minstarlifetime = (long)(floatval*1000000.0f);
			return true;
		}
	}
	else if (!strcmp(paramtoset,"maxstarlifetime"))
	{
		int ret = sscanf(message,"%23s %f",&paramtoset[0],&floatval);
		if (ret == 2)
		{
			if (floatval < 0 || floatval > 20) return false;
			Serial.println("Setting!");
			maxstarlifetime = (long)(floatval*1000000.0f);
			return true;
		}
	}

	else if (!strcmp(message, "print"))
	{
		PrintTunables();
		return true;
	}
	else if (!strcmp(message, "write"))
	{
		WriteTunablesToEeprom();
		return true;
	}
	else if (!strcmp(message, "read"))
	{
		ReadTunablesFromEeprom();
		return true;
	}
	// Some messages are not technically "tunables" but need to be passed down the chain anyway
	// These are handled by simply returning "true" and causing the retransmit to all panels/edges
	// whichever panels or edges recognize them will handle them appropriately
	else if (!strcmp(message,"rainmode"))
	{
		return true;
	}
	else if (!strcmp(message,"burstmode"))
	{
		return true;
	}

	return false;
}

