#ifndef REMOTE_CONTROLLER
#define REMOTE_CONTROLLER
#include <arduino.h>
#include "Controller.h"
#include "timer.h"
#include "CommsRouter.h"
#include "MaxCoat.h"
#include "MaxBike.h"
#include "DianeCoat.h"

class RemoteController : Controller
{
	// A timer for max and dianes coats to send messages to the master
	Timer coatTimer;

	bool nearStructure;

	MaxCoat maxCoatEffect;
	MaxBike maxBikeEffect;
	DianeCoat dianeCoatEffect;

	public:
		RemoteController(uint16_t id, const char* name);

		// Update will process any inputs (sensors, etc), perform comms on the radios, process any commands in the command queue
		void Update(unsigned long deltamics);

		// Do any post-construction setup (network initialize etc)
		void Initialize();

		virtual void HandleMessage(const char* message, bool fromSerial);

		// Render will take the current state of simulation and render it to the led strips (if any)
		void Render();
		// Reset will take us back to the state just after power on (typically only used if something goes wrong)
		void Reset();
};

#endif
