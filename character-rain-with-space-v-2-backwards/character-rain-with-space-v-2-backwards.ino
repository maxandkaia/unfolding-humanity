

#define DECLARE_GLOBALS

// Comment this in/out to turn on/off debug print diagnostics
#define DEBUG
#include "debug_utils.h"
#undef DEBUG

#include "leds.h"
#include "tunables.h"
#undef DECLARE_GLOBALS

#include "timer.h"

// physical setup constants
#include "constants.h"

// Utilities
#include "utils.h"
#include "Controller.h"
#include "TopEdgeController.h"
#include "TopPanelController.h"
#include "MasterController.h"
#include "RemoteController.h"

#include "Dodecahedron.h"

#include "palettes.h"
#include "palmixer.h"

#include "RamMonitor.h"


RamMonitor ram;
uint32_t   reporttime;

void report_ram_stat(const char* aname, uint32_t avalue) {
  Serial.print(aname);
  Serial.print(": ");
  Serial.print((avalue + 512) / 1024);
  Serial.print(" Kb (");
  Serial.print((((float) avalue) / ram.total()) * 100, 1);
  Serial.println("%)");
};

void report_ram() {
  bool lowmem;
  bool crash;
  
  Serial.println("==== memory report ====");
  
  report_ram_stat("free", ram.adj_free());
  report_ram_stat("stack", ram.stack_total());
  report_ram_stat("heap", ram.heap_total());
  
  lowmem = ram.warning_lowmem();
  crash = ram.warning_crash();
  if(lowmem || crash) {
    Serial.println();
    
    if(crash)
      Serial.println("**warning: stack and heap crash possible");
    else if(lowmem)
      Serial.println("**warning: unallocated memory running low");
  };
  
  Serial.println();
};


//!!!!!!!!!! NOTE !!!!!!!!!!!!
// Set the controller type here before compiling and uploading to a controller!!
// the possible controller variants are in the constants.h file.
//!!!!!!!!!! NOTE !!!!!!!!!!!!

//uint16_t controllerVariant  = REMOTE_ID;
//uint16_t controllerVariant  = MAXS_COAT;
//uint16_t controllerVariant  = MAXS_BIKE;
//uint16_t controllerVariant  = DIANES_COAT;


// Set this to be one of the possible controller variants from the constants.h file. 
//uint16_t controllerVariant = MASTER_ID;

//uint16_t controllerVariant = TOP_EDGE_1;
//uint16_t controllerVariant = TOP_EDGE_2;
//uint16_t controllerVariant = TOP_EDGE_3;
//uint16_t controllerVariant = TOP_EDGE_4;
uint16_t controllerVariant = TOP_EDGE_5;

//uint16_t controllerVariant = TOP_PANEL_1_A;
//uint16_t controllerVariant = TOP_PANEL_1_B;

//uint16_t controllerVariant = TOP_PANEL_2_A;
//uint16_t controllerVariant = TOP_PANEL_2_B;

//uint16_t controllerVariant = TOP_PANEL_3_A;
//uint16_t controllerVariant = TOP_PANEL_3_B;

//uint16_t controllerVariant = TOP_PANEL_4_A;
//uint16_t controllerVariant = TOP_PANEL_4_B;

//uint16_t controllerVariant = TOP_PANEL_5_A;
//uint16_t controllerVariant = TOP_PANEL_5_B;

//uint16_t controllerVariant = BOTTOM_PANEL_1_A;
//uint16_t controllerVariant = BOTTOM_PANEL_1_B;

//uint16_t controllerVariant = BOTTOM_PANEL_2_A;
//uint16_t controllerVariant = BOTTOM_PANEL_2_B;

//uint16_t controllerVariant = BOTTOM_PANEL_3_A;
//uint16_t controllerVariant = BOTTOM_PANEL_3_B;

//uint16_t controllerVariant = BOTTOM_PANEL_4_A;
//uint16_t controllerVariant = BOTTOM_PANEL_4_B;

//uint16_t controllerVariant = BOTTOM_PANEL_5_A;
//uint16_t controllerVariant = BOTTOM_PANEL_5_B;

Controller* controller;

unsigned long lastmics = 0;

// Timers
Timer printTimer;
Timer updateTimer;
Timer palshiftTimer;

// ============ Utility for creating the right kind of controller and assigning network IDs ==========
//====================================================================================================
Controller* CreateController(uint16_t controller_id)
{
  const char* name;
  switch (controller_id)
  {
    case REMOTE_ID:
    name = "REMOTE_CONTROL";
    return (Controller*) new RemoteController(controller_id, name);

    case DIANES_COAT:
    name = "DIANES_COAT";
    return (Controller*) new RemoteController(controller_id, name);

    case MAXS_COAT:
    name = "MAXS_COAT";
    return (Controller*) new RemoteController(controller_id, name);

    case MAXS_BIKE:
    name = "MAXS_BIKE";
    return (Controller*) new RemoteController(controller_id, name);

    case MASTER_ID:
    name = "MASTER";
    return (Controller*) new MasterController(controller_id, name);

    case TOP_EDGE_1:
    name = "TOP_EDGE_1";
    return (Controller*) new TopEdgeController(controller_id, name);

    case TOP_EDGE_2:
    name = "TOP_EDGE_2";
    return (Controller*) new TopEdgeController(controller_id, name);

    case TOP_EDGE_3:
    name = "TOP_EDGE_3";
    return (Controller*) new TopEdgeController(controller_id, name);

    case TOP_EDGE_4:
    name = "TOP_EDGE_4";
    return (Controller*) new TopEdgeController(controller_id, name);

    case TOP_EDGE_5:
    name = "TOP_EDGE_5";
    return (Controller*) new TopEdgeController(controller_id, name);

    case TOP_PANEL_1_A:
    name = "TOP_PANEL_1_A";
    return (Controller*) new TopPanelController(controller_id, name);

    case TOP_PANEL_1_B:
    name = "TOP_PANEL_1_B";
    return (Controller*) new TopPanelController(controller_id, name);


    case TOP_PANEL_2_A:
    name = "TOP_PANEL_2_A";
    return (Controller*) new TopPanelController(controller_id, name);

    case TOP_PANEL_2_B:
    name = "TOP_PANEL_2_B";
    return (Controller*) new TopPanelController(controller_id, name);

    case TOP_PANEL_3_A:
    name = "TOP_PANEL_3_A";
    return (Controller*) new TopPanelController(controller_id, name);

    case TOP_PANEL_3_B:
    name = "TOP_PANEL_3_B";
    return (Controller*) new TopPanelController(controller_id, name);

    case TOP_PANEL_4_A:
    name = "TOP_PANEL_4_A";
    return (Controller*) new TopPanelController(controller_id, name);

    case TOP_PANEL_4_B:
    name = "TOP_PANEL_4_B";
    return (Controller*) new TopPanelController(controller_id, name);

    case TOP_PANEL_5_A:
    name = "TOP_PANEL_5_A";
    return (Controller*) new TopPanelController(controller_id, name);

    case TOP_PANEL_5_B:
    name = "TOP_PANEL_5_B";
    return (Controller*) new TopPanelController(controller_id, name);


// For now, the bottom panel controllers all use the exact same code/class as top panels
    case BOTTOM_PANEL_1_A:
    name = "BOTTOM_PANEL_1_A";
    return (Controller*) new TopPanelController(controller_id, name);

    case BOTTOM_PANEL_1_B:
    name = "BOTTOM_PANEL_1_B";
    return (Controller*) new TopPanelController(controller_id, name);

    case BOTTOM_PANEL_2_A:
    name = "BOTTOM_PANEL_2_A";
    return (Controller*) new TopPanelController(controller_id, name);

    case BOTTOM_PANEL_2_B:
    name = "BOTTOM_PANEL_2_B";
    return (Controller*) new TopPanelController(controller_id, name);

    case BOTTOM_PANEL_3_A:
    name = "BOTTOM_PANEL_3_A";
    return (Controller*) new TopPanelController(controller_id, name);

    case BOTTOM_PANEL_3_B:
    name = "BOTTOM_PANEL_3_B";
    return (Controller*) new TopPanelController(controller_id, name);

    case BOTTOM_PANEL_4_A:
    name = "BOTTOM_PANEL_4_A";
    return (Controller*) new TopPanelController(controller_id, name);

    case BOTTOM_PANEL_4_B:
    name = "BOTTOM_PANEL_4_B";
    return (Controller*) new TopPanelController(controller_id, name);

    case BOTTOM_PANEL_5_A:
    name = "BOTTOM_PANEL_5_A";
    return (Controller*) new TopPanelController(controller_id, name);

    case BOTTOM_PANEL_5_B:
    name = "BOTTOM_PANEL_5_B";
    return (Controller*) new TopPanelController(controller_id, name);

    default: 
    // Error!
    return NULL;
    break;

  }
}
//=====================================================================




//--------------------------- SETUP() ---------------------------------

void setup() { 

  //ram.initialize();

  
  reporttime = millis();


  Serial.begin(SERIAL_RATE);
  Serial.println("Serial begin... hopefully the COM port stays up");

  TunablesManager::SetTunableDefaults();

  controller = CreateController(controllerVariant);

  controller->Initialize();

  printTimer.SetFrequency(0.1f); //print diagnostics every 10 seconds
  printTimer.Start();

  updateTimer.SetFrequency(60.0f); // update and render at 60FPS
  updateTimer.Start();  

  //delay(1000); // Wait for a bit so we can get on the monitor and watch the spin up sequence...

  //Dodecahedron::Initialize();

  palshiftTimer.SetInterval(10.0f);
  palshiftTimer.Start();

  //Serial.println("Generating palettes...");
  palmixer::GenerateGlobalPalettes();
  //Serial.println("Done generating palettes");

  // Randomize the randomizer
  randomSeed(analogRead(0));

  // Stagger a random-ish delay. Might help reduce comms collisions?
  controller->router->radio->powerDown();
  delay(controller->my_address * 10);
  controller->router->radio->powerUp();

}

static const int MAX_BUF = 64;
char inputBuffer[MAX_BUF];
int numInput = 0;

bool ProcessInput(unsigned long mics)
{

    while (Serial.available() > 0) {
            // read the incoming byte:
      char c = Serial.read();
      if (c == '\n')
      {
        inputBuffer[numInput++] = 0;
        //Serial.println("command entered!");
        controller->HandleMessage(&inputBuffer[0], true);
        numInput = 0;
        return true;
      }
      else if (numInput < MAX_BUF)
      {
            inputBuffer[numInput++] = c;
      }
      else
      {
        Serial.println("Input buffer overflow!");
        numInput = 0;
      }
    }
    return false;
}

//--------------------------- LOOP() ---------------------------------
int loop_count = 0;
unsigned long micsthisupdate = 0;

long maxmics = 0;
long minmics = 1000000;

int curPaletteIndex = 0;

void loop() { 


  // Do timing work
  unsigned long curmics = micros();
  
  // Handle rollover
  if (curmics < lastmics)
    lastmics = curmics;

// Get a delta microseconds measure for this frame
  unsigned long deltamics = curmics - lastmics;
  lastmics = curmics;
  micsthisupdate += deltamics;

// Update all timers
  printTimer.Update(deltamics);
  updateTimer.Update(deltamics);
  palshiftTimer.Update(deltamics);

// Print out some diagnostics every once in a while
  if (printTimer.GetEvent())
  {
    // Print any other loop statistics here...
    Serial.print("FPS: "); Serial.println((float)(loop_count)/10.0f);
    Serial.print("Max mics was: "); Serial.println(maxmics);
    Serial.print("Min mics was: "); Serial.println(minmics);
    loop_count = 0;
    maxmics = 0;
    minmics = 1000000;
  }

// shift to new palette when the time is right
  if (palshiftTimer.GetEvent())
  {
    //Serial.println("Shifting to new palette...");
    int nextPaletteIndex = random(0,palmixer::kNumPalettes);
    // Select next palette and blend over 3.0 seconds
    palmixer::SetNewPalette(nextPaletteIndex, 3.0f);
  }

// Check for serial command inputs
  ProcessInput(deltamics);

  //DPRINT("About to call update on the controller...");

// Do our 60 FPS updates to the whole system
  if (updateTimer.GetEvent())
  {
    //Serial.println("Update...");

    // Update the palette mixer at 1/60th of a second
    palmixer::Update(0.016f);

    //Serial.println("Done updating palmixer");

    unsigned long frameStart = micros();

    controller->Update(micsthisupdate);

    //Serial.println("after update and before Render");
    controller->Render();

    //Serial.println("Done rendering");

    unsigned long frameEnd = micros();
    unsigned long frameMics = frameEnd - frameStart;
    //Serial.print("Mics: "); Serial.println(frameMics);

    //DPRINT("About to call render on the controller...");
    if (frameMics > maxmics)
      maxmics = frameMics;
    if (frameMics < minmics)
      minmics = frameMics;


    micsthisupdate = 0;
    loop_count++;
  }
}

  
