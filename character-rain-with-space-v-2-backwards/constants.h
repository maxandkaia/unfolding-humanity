#ifndef CONSTANTS
#define CONSTANTS

#define NUM_PANEL_STRIPS 8
#define LEDS_PER_PANEL_STRIP 120
#define PIXELS_PER_CHAR 6
#define CHARS_PER_PANEL_STRIP LEDS_PER_PANEL_STRIP / PIXELS_PER_CHAR

// One "segment" is controlled by a single TOP_EDGE controller. A segment consists of a top edge,
// then a descending edge, two transverse edges, one in each of the equatorial directions, then another
// descending edge, then a bottom edge. The exact order and mapping of these edges to the led array is still
// TBD, but we will allocate enough space for all leds
#define NUM_LEDS_PER_EDGE 42
#define NUM_EDGES_PER_SEGMENT 6
#define NUM_LEDS_PER_EDGE_CHANNEL NUM_LEDS_PER_EDGE*NUM_EDGES_PER_SEGMENT

#define SERIAL_RATE 57600

// Might want to make this tunable and launch multiple drops per strip (at different speeds even)
#define MAX_DROPS NUM_PANEL_STRIPS


// Place all constants here


// Some identifiers for networking and comms. Controller addresses are OCTAL constants.

// Since we already have master as 00, and all children of that are occupied (top edge controllers)
// we will make the remotes children of top edge controller #1
#define REMOTE_ID 0051
#define DIANES_COAT 0054
#define MAXS_COAT 0052
#define MAXS_BIKE 0053

#define MASTER_ID 00

#define TOP_EDGE_1 001
#define TOP_EDGE_2 002
#define TOP_EDGE_3 003
#define TOP_EDGE_4 004
#define TOP_EDGE_5 005

#define TOP_PANEL_1_A 0011
#define TOP_PANEL_1_B 0021

#define TOP_PANEL_2_A 0012
#define TOP_PANEL_2_B 0022

#define TOP_PANEL_3_A 0013
#define TOP_PANEL_3_B 0023

#define TOP_PANEL_4_A 0014
#define TOP_PANEL_4_B 0024

#define TOP_PANEL_5_A 0015
#define TOP_PANEL_5_B 0025

#define BOTTOM_PANEL_1_A 0031
#define BOTTOM_PANEL_1_B 0041

#define BOTTOM_PANEL_2_A 0032
#define BOTTOM_PANEL_2_B 0042

#define BOTTOM_PANEL_3_A 0033
#define BOTTOM_PANEL_3_B 0043

#define BOTTOM_PANEL_4_A 0034
#define BOTTOM_PANEL_4_B 0044

#define BOTTOM_PANEL_5_A 0035
#define BOTTOM_PANEL_5_B 0045


#endif