#ifndef RAINDROP
#define RAINDROP

/// Includes
#include <arduino.h>
#include "FastLED.h"
#include "timer.h"

// A raindrop is a single position on the character board. It moves down at a given speed, and the deactivates when it reaches the bottom
class raindrop
{


	int position; // from top to bottom, starting at 0 and descending to 20 (the virtual character sheet is 21 lanes wide and 21 "characters" tall)
	CRGB color;
	CRGB trailcolor;
	float speed; // in character positions per second
	Timer shiftTimer;
	bool reverse;


public:
	bool active;
	uint8_t stripnum; // which strip it is in	
	
	//==============================
// Some utilities for the face panel

// Face panel is a virtual panel. Not all strips actually extend the full vertical span. Some are shorter, but for ease
// and simplicity, we have defined all the led strip lengths to be the same, the maximum length. We can simulate the characters 
// in the virtual space and when they arrive at a place that has actual physical leds, they will show up
// The virtual board is 21 lanes wide (stripnum) and 21 characters high (from 0 at the top to 20 at the bottom)
// Each character is 5 leds high, with 1 unused led between each


// Light up a character on the board in the given position with a given color
void LightCharacter(uint8_t stripnum, int charposition, CRGB color);

void FadeAllStrips(uint8_t fadefactor);

//==========================


	void Launch(uint8_t _stripnum, int _position, float _speed, CRGB _color, CRGB _trailcolor, bool reverse);
	void Update(unsigned long deltamics);
	void MoveCharacterDown();
};


#endif