#include "number_utils.h"
#include <arduino.h>

namespace utils
{

float frand(float LO, float HI)
{
  float r3 = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
  return r3;
}

}
