
#include <arduino.h>
#include "StatusTracker.h"
#include "Constants.h"


		StatusTracker::StatusTracker()
		{
			statusTrackers[0].Init("TOP_EDGE_1",001);
			statusTrackers[1].Init("TOP_EDGE_2",002);
			statusTrackers[2].Init("TOP_EDGE_3",003);
			statusTrackers[3].Init("TOP_EDGE_4",004);
			statusTrackers[4].Init("TOP_EDGE_5",005);

			statusTrackers[5].Init("TOP_PANEL_1_A",0011);
			statusTrackers[6].Init("TOP_PANEL_1_B",0021);

			statusTrackers[7].Init("TOP_PANEL_2_A",0012);
			statusTrackers[8].Init("TOP_PANEL_2_B",0022);

			statusTrackers[9].Init("TOP_PANEL_3_A",0013);
			statusTrackers[10].Init("TOP_PANEL_3_B",0023);

			statusTrackers[11].Init("TOP_PANEL_4_A",0014);
			statusTrackers[12].Init("TOP_PANEL_4_B",0024);

			statusTrackers[13].Init("TOP_PANEL_5_A",0015);
			statusTrackers[14].Init("TOP_PANEL_5_B",0025);

			statusTrackers[15].Init("BOTTOM_PANEL_1_A",0031);
			statusTrackers[16].Init("BOTTOM_PANEL_1_B",0041);

			statusTrackers[17].Init("BOTTOM_PANEL_2_A",0032);
			statusTrackers[18].Init("BOTTOM_PANEL_2_B",0042);

			statusTrackers[19].Init("BOTTOM_PANEL_3_A",0033);
			statusTrackers[20].Init("BOTTOM_PANEL_3_B",0043);

			statusTrackers[21].Init("BOTTOM_PANEL_4_A",0034);
			statusTrackers[22].Init("BOTTOM_PANEL_4_B",0044);

			statusTrackers[23].Init("BOTTOM_PANEL_5_A",0035);
			statusTrackers[24].Init("BOTTOM_PANEL_5_B",0045);
		}

		// Increment everyones counter. They get cleared when HBs are received.
		void StatusTracker::Update(unsigned long deltamics)
		{float secs = (float)deltamics/1000000.0f;
			for (int i=0; i<25; i++)
			{
				statusTrackers[i].secs_since_last_hb += secs;
			}
		}

		// Call this to let us know a controller has reported in with a HB
		void StatusTracker::UpdateStatus(uint16_t controller_id)
		{
			for (int i=0; i<25; i++)
			{
				if (statusTrackers[i].controller_id == controller_id)
					statusTrackers[i].secs_since_last_hb = 0.0f;
			}			
		}

		// If any controller has not successfully HB'ed in 5 seconds, something is down! This
		// function reports true if all are online, false if ANY are down 
		bool StatusTracker::AllControllersOnline()
		{
			for (int i=0; i<25; i++)
			{
				if (statusTrackers[i].secs_since_last_hb > 5.0f)
					return false;
			}
			return true;
		}

		// This function sends the report to the serial output and also to the remote controller
		void PrintAndSend(const char* s)
		{
			Serial.println(s);
			// <<TODO>> Give this class access to the controller for comms
		}

		void StatusTracker::Report()
		{
			PrintAndSend("=========== Controller Status Report ===========");
			char statrep[64];
			for (int i=0; i<25; i++)
			{
				sprintf(statrep,"Controller id: %s     Seconds since last HB:  %f", statusTrackers[i].controller_name, statusTrackers[i].secs_since_last_hb);
				PrintAndSend(statrep);
			}
			PrintAndSend("========== Done with Status Report  ============");
		}

