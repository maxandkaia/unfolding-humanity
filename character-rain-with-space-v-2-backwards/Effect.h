#ifndef EFFECT
#define EFFECT

// The raindrops that we will be managing
#include <arduino.h>
#include "constants.h"
#include "tunables.h"
#include "timer.h"

// The Effect class is just a base class for all effects so that they share the same basic pattern and update scheme
// You create one, Init() it, then Update(dt), then Render()
// This way, we can have multiple effects and choose between them (only update and render the active one)
// Later, we can add ways to transition between effects and/or have multiple active at one time and blend the rendering of them etc.
class Effect
{


public:
	virtual void Init();

	virtual void Update(unsigned long deltamics);

	virtual void Render();
};


#endif