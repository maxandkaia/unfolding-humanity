#include "Dodecahedron.h"
#include <math.h>
#include <arduino.h>
#include "constants.h"

// Commenting the entire class out for now, as we need the RAM for other things
#if 0

//#define DEBUG
#include "debug_utils.h"
//#undef DEBUG

// Class and methods for creating useful geometric access to the dodecahedron

	// CONVENTIONS
	// 1) 'around the clock' is from the POV of above the dodec looking down
	// 2) the shape is centered at the R3 origin
	// 3) the radius of the shape (distance from origin to any vertex) is SQRT(3.0)
	// 
	// 
 //
 // The vertices of the dodecahedron, indexed in a specific order which goes as follows:
 // The five vertices around the top face, 0..4, and let's call vertex 0 12 oclock looking down at the shape from above
 // Then, all the 'middle' vertices, i.e. all not either on the top face or the bottom face, 5..14, going around the clock in 
 // a clockwise fashion, starting with the vertex that is most immediately clockwise from our 12 oclock top face vertex
 // then, the 'bottom face' vertices, 15..19, also going clockwise starting again with the vertex that is most immediately clockwise
 // from our 12 oclock top vertex when looking down from above
 // 
	//static vec3 vertices[20];


	//static int top[5]; // ordered around the clock from noon
	//static int middle[10]; // ordered around the clock
	//static int bottom[5]; // ordered around the clock

	//
	// Each vertex has 3 neighbors. Let's call these the clockwise, anticlockwise and up/down neighbors. Each neighbor can be classified as one
	// of these categories by virtue of whether or not it is in the same 'level' top, middle, bottom} or in a different one. If it is in the same
	// level, then it is either a clockwise, or anticlockwise neighbor. We will use the convention that the clockwise neighbor comes first, followed
	// by the anticlockwise neighbor, followed by the up/down neighbor. It is the case that two of the neighbors will always be
	// clockwise/anticlockwise (these will be neighbors 0 and 1, respectively) and ONE will be a top/botom neighbor. All vertices in the top face
	// will have one bottom neighbor. All vertices in the bottom face will have one top neighbor, and all middle vertices will have one or the other,
	// alternating as you wind around the clock. Thus, if we drew a line from each vertex to it's two clockwise/anticlockwise neighbors, we would see
	// three outlines drawn on the shape. The perimeter of the top face, the perimeter of the bottom face, and a zig-zag line around the middle of
	// the shape. 
	//

	//static int adjacency[3][20]; // 20 sets of adjacency information ordered as per comments above

	// We use the same scheme to catalog the edges. Each edge is a list of two vertices (by index) and we start at the top face, going clockwise
	// from our 12 oclock vertex, we have edges 0..4. Then, we have 'descending' edges from each of those vertices, starting with our 12 oclock
	// vertex. Then, we have the zig-zag middle belt of edges which connect the middle vertices, starting with the middle vertex most immediately
	// clockwise from our 12 oclock top vertex. Then we have descending edges that connext the middle belt with the bottom face, again starting with
	// the vertex most immediately clockwise from our 12 oclock top vertex. Then we have the bottom face perimeter edges, starting with the bottom
	// vertex that is most immediately clockwise from our 12 oclock vertex.
	//static int edges[2][30];

// Allocate storage for the static class vars
	vec3 Dodecahedron::vertices[20];
	int Dodecahedron::top[5]; // ordered around the clock from noon
	int Dodecahedron::middle[10]; // ordered around the clock
	int Dodecahedron::bottom[5]; // ordered around the clock
	int Dodecahedron::adjacency[3][20]; // 20 sets of adjacency information ordered as per comments above
	int Dodecahedron::edges[60] = {
			0,1,
			1,6,
			6,7,
			7,8,
			7,16,
			16,17,

			1,2,
			2,8,
			8,9,
			9,10,
			9,17,
			17,18,

			2,3,
			3,10,
			10,11,
			11,12,
			11,18,
			18,19,

			3,4,
			4,12,
			12,13,
			13,14,
			13,19,
			19,15,

			4,0,
			0,14,
			14,5,
			5,6,
			5,15,
			15,16
		}; // see the VerticesLayout.png file in the project folder for diagram of edge numbering and layout

	vec3 Dodecahedron::segverts[5][NUM_LEDS_PER_EDGE_CHANNEL]; // A list of LED locations along each of the "segments"



	// small print utility
	void PrintVert(vec3 v)
	{
	Serial.print(v.x); Serial.print(", ");
	Serial.print(v.y); Serial.print(", ");
	Serial.println(v.z);
	}


	// Method to compute and initialize all the above data structures
	void Dodecahedron::Initialize()
	{
		Serial.println("================= Dodecahedron Initialize =================");
		// Given A the golden ratio} = 1.618
		// The 'canonical' i.e. as per Wolfram and Wikipedia} vertices of the solid have coordinates 
		// {+/-1, +/-1, +/-1}
		// {0, +/-A, +/-1/A}}
		// {+/-1/A}, 0, +/-A}
		// {+/-A, +/-1/A}, 0}
		// 
		// These are in no particular order, and belong to a dodecahedron that is not 'laying flat', i.e. does not have one
		// face lying flush to the ground, as we will have ours. So, I will rotate the mesh around the origin so that it does lie
		// flat using this technique:
		// Pick 3 vertices that are known to be on a face any three from any face will do}. Use these to define a plane equation. 
		// Find an transform which re-orients this plane so that its perpendicular is coincident with R3 Y axis the up vector for us}
		// Apply this transform to all vertices. Once transformed, separate the verts into the 3 layers top, middle, bottom}. Then, 
		// using their projections onto the XZ plane the ground} sort them 'around the clock' using simple trig. Use these sortings to
		// populate our vertex array and our edge and adjacency arrays

		// Alpha
		float A = 1.618f;
		float invA = 1.0f/A;

		// Canonical verts:
		vec3 canonicalVerts[20] = 
		{
			{1.0f,1.0f,1.0f},
			{1.0f,1.0f,-1.0f},
			{1.0f,-1.0f,1.0f},
			{1.0f,-1.0f,-1.0f},
			{-1.0f,1.0f,1.0f},
			{-1.0f,1.0f,-1.0f},
			{-1.0f,-1.0f,1.0f},
			{-1.0f,-1.0f,-1.0f},

			{0.0f, A, invA},
			{0.0f, A, -invA},
			{0.0f, -A, invA},
			{0.0f, -A, -invA},

			{invA, 0.0f, A},
			{invA, 0.0f, -A},
			{-invA, 0.0f, A},
			{-invA, 0.0f, -A},

			{A, invA, 0.0f},
			{A, -invA, 0.0f},
			{-A, invA, 0.0f},
			{-A, -invA, 0.0f}
		};

		vec3 rotatedVerts[20];
		vec3 bottomVerts[5];
		vec3 middleVerts[10];
		vec3 topVerts[5];

		float headings[20]; // the clockwise orientations of the verts as projected onto the XZ plane and relative to the +Z axis, in radians

		// Pick three verts that are near the top. These would be the two with Y coordinates of +A, and one with 
		// Y coord of +1. Let's pick verts 8,9,0. Make a triangle out of these and get the plane equation
		int I = 8;
		int J = 9;
		int K = 0;

		vec3 YAxis = {0.0f, 1.0f, 0.0f};

		vec3 vectorA, vectorB;
		SubVec3(&vectorA, &canonicalVerts[I], &canonicalVerts[J]);
		SubVec3(&vectorB, &canonicalVerts[K], &canonicalVerts[J]);
		NormalizeVec3_Self(&vectorA);
		NormalizeVec3_Self(&vectorB);
		// Cross the two to get the perpendicular
		vec3 vectorC;
		CrossVec3(&vectorC, &vectorA, &vectorB);
		// make sure we normalize again to combat rounding error
		NormalizeVec3_Self(&vectorC);

		// Now, create a transform that rotates vectorC so it is parallel to the Y axis
		// Since the simple math lib I am using does not have full 3D transform utils, we will do this
		// kind of old-school. We will find the axis of rotation using the cross product of C and the Y axis
		// then find the angle of rotation using the dot product, and then use the utility for rotating
		// around an axis (which the lib DOES have :) )
		vec3 rotationAxis;
		CrossVec3(&rotationAxis, &vectorC, &YAxis);
		NormalizeVec3_Self(&rotationAxis); // for safety sake

		float rotationAngleCosine = DotVec3(&vectorC, &YAxis);
		float rotationAngleRadians = acos(rotationAngleCosine);
		float rotationAngleSin = sin(rotationAngleRadians);

		// Let's test our rotation factors to see if it works
		vec3 yAxisTest;
		RotateVec3AboutAxis(&yAxisTest, &vectorC, &rotationAxis, rotationAngleSin, rotationAngleCosine);
		// the test vector better be the Y axis now

		float testDot = DotVec3(&yAxisTest, &YAxis);
		if (testDot < 0.99f || testDot > 1.01f)
		{
			Serial.println("Error generating rotation for geometry setup\n");
		}

		// Assuming all went well, go ahead and rotate all the canonical verts into a rotated verts array
		// and check them as well
		for (int i=0; i<20; i++)
		{
			RotateVec3AboutAxis(&rotatedVerts[i], &canonicalVerts[i], &rotationAxis, rotationAngleSin, rotationAngleCosine);
		}


		// Now we need to sort the verts by order 'around the clock'. 12 oclock will be the Z axis for now.
		for (int i=0; i<20; i++)
		{
			vec2 direction = {rotatedVerts[i].x, rotatedVerts[i].z};
			NormalizeVec2_Self(&direction);
			double angleInRadians = atan2(direction.x, direction.y);
			if (angleInRadians <= 0.0f)
				angleInRadians += PI*2.0f;
			headings[i] = (float)angleInRadians;
		}
		// Use bubblesort cuz we don't care about performance and just want simple code
		for (int i = 0; i < 19; i++)      
		{
		   // Last i elements are already in place   
		   for (int j = 0; j < 19-i; j++) 
		   {
		       if (headings[j] > headings[j+1])
		       {
		          vec3 temp;
		          float ftemp;
		          ftemp = headings[j];
		          headings[j] = headings[j+1];
		          headings[j+1] = ftemp;
		          temp = rotatedVerts[j];
		          rotatedVerts[j] = rotatedVerts[j+1];
		          rotatedVerts[j+1] = temp;
		       }
		   }
		}

		Serial.println("Sorted headings:");
		for (int i=0; i<20; i++)
		{
			Serial.print("Heading: "); Serial.println(headings[i]);
		}

		// NOTE: All the verts are now sorted going around the clock. The next few passes will maintain this order and simply
		// separate them into 3 lists, but clock ordering will be maintained


		// Separate them into 3 layers (top middle bottom) and print them out
		float minY = 100.0f;
		float maxY = -100.0f;
		float minX = 100.0f;
		float maxX = -100.0f;
		float minZ = 100.0f;
		float maxZ = -100.0f;

		for (int i=0; i<20; i++)
		{
			if (rotatedVerts[i].x < minX)
				minX = rotatedVerts[i].x;
			if (rotatedVerts[i].x > maxX)
				maxX = rotatedVerts[i].x;

			if (rotatedVerts[i].y < minY)
				minY = rotatedVerts[i].y;
			if (rotatedVerts[i].y > maxY)
				maxY = rotatedVerts[i].y;

			if (rotatedVerts[i].z < minZ)
				minZ = rotatedVerts[i].z;
			if (rotatedVerts[i].z > maxZ)
				maxZ = rotatedVerts[i].z;
		}

		// Scale all the data down so that our shape fits inside the bi-unit cube (-1..1)
		float xScale = 2.0f/(maxX-minX);
		float yScale = 2.0f/(maxY-minY);
		float zScale = 2.0f/(maxZ-minZ);

		// We actually want to do a uniform scale so as not to change the shape, but the main goal of the scale
		// operation is to ensure that our coordinate data does not extend out past -1.0 or 1.0, and that way
		// we can easily/conveniently calculate color values from coordinate values. So, we will find the min
		// scale value from the above non-uniform set of scalars, and use that. This will guarantee that our shape
		// lives inside the bi-unit cube and we won't have to worry about overflow when scaling coords to colors
		float uniformScale = min(min(xScale,yScale),zScale);
		for (int i=0; i<20; i++)
		{
			ScaleVec3_Self(&rotatedVerts[i],uniformScale);
		}

		// Ok, now the vertex data should be in normalized form, we can proceed

		int curBot = 0;
		int curMid = 0;
		int curTop = 0;
		for (int i=0; i<20; i++)
		{
			if (rotatedVerts[i].y < -0.8f)
			{
				if (curBot == 5)
				{
					Serial.println("Error in sorting verts into layers!");
					break;
				}
				bottomVerts[curBot++] = rotatedVerts[i];
			}
			else if (rotatedVerts[i].y > 0.8f)
			{
				if (curTop == 5)
				{
					Serial.println("Error in sorting verts into layers!");
					break;
				}
				topVerts[curTop++] = rotatedVerts[i];
			}
			else
			{
				if (curMid == 10)
				{
					Serial.println("Error in sorting verts into layers!");
					break;
				}
				middleVerts[curMid++] = rotatedVerts[i];
			}
		}

		Serial.print("Num top verts: "); Serial.println(curTop);
		Serial.print("Num mid verts: "); Serial.println(curMid);
		Serial.print("Num bot verts: "); Serial.println(curBot);
		Serial.print("Min Y: "); Serial.println(minY);
		Serial.print("Max Y: "); Serial.println(maxY);

		Serial.println("");
		Serial.println("Top verts:");

		// At the same time we do this, we will rearrange the rotated verts list to match the diagram
		int rotIndex = 0;
		for (int i=0; i<5; i++)
		{
			PrintVert(topVerts[i]);
		}

		Serial.println("");
		Serial.println("Middle verts:");

		for (int i=0; i<10; i++)
		{
			PrintVert(middleVerts[i]);
		}

		Serial.println("");
		Serial.println("Bottom verts:");

		for (int i=0; i<5; i++)
		{
			PrintVert(bottomVerts[i]);
		}


		// Now populate the vertices array in order of topverts, middleverts, bottomverts
		// We don't really need the indices as they are in this specific order. If you want middle vertex 1, its vertices[6]
		int cur = 0;
		for (int i=0; i<5; i++)
		{			
			vertices[cur++] = topVerts[i];
		}

		for (int i=0; i<10; i++)
		{			
			vertices[cur++] = middleVerts[i];
		}

		for (int i=0; i<5; i++)
		{
			vertices[cur++] = bottomVerts[i];
		}

		// Now for the adjacency lists. This is trickier!
		// Ok, I am just going to brute force this. For each vertex, check the other verts. If one of them is within edgelength + margin distance, then it is a neighbor
		float edgelength = 1.401f;
		float margin = 0.5f;

		for (int i=0; i<20; i++)
		{
			vec3 testPoint = vertices[i];
			int numNeighbors = 0;

			for (int j=0; j<20; j++)
			{
				if (j != i)
				{
					float dist = DistVec3(&testPoint, &vertices[j]);
					if (dist < (edgelength + margin) )
					{
						Dodecahedron::adjacency[numNeighbors++][i] = j;
					}
				}
			}

			if (numNeighbors != 3)
				Serial.println("Error in adjacency calculations!");
		}

		// Print out the adjacency info
		for (int i=0; i<20; i++)
		{
			Serial.print(i); Serial.print(": "); Serial.print(adjacency[0][i]); Serial.print(", "); Serial.print(adjacency[1][i]); Serial.print(", "); Serial.println(adjacency[2][i]);;
		}

		Serial.println("Done with adjacency calculations");
		Serial.flush();

		// Now set up the led locations. See the VerticesLayout.png file in the project folder for details about the layout. The first
		// 6 edges constitute "segment 0", the next 6 are "segment 1", where a segment is controlled by a single controller.
		// We may not have LEDS on the 6th edge (the bottom edge) of each segment, but we will have the geometry data for them
		// anyway

		for (int seg=0; seg<5; seg++)
		{
			// The index into the edges array of the first edge of this segment
			//Serial.print("Segment: "); Serial.println(seg);

			int firstEdge = seg*6;
			int lastEdge = firstEdge + 5;
			int ledIndex = 0;
			for (int edge=firstEdge; edge<=lastEdge; edge++)
			{
				//Serial.print("Edge: "); Serial.println(edge);

				// Get the vertices of this edge
				int i0 = edges[edge*2 + 0];
				int i1 = edges[edge*2 + 1];

				//Serial.print("i0: "); Serial.println(i0);
				//Serial.print("i1: "); Serial.println(i1);

				vec3 v0 = vertices[i0];
				vec3 v1 = vertices[i1];

				PrintVert(v0);
				PrintVert(v1);

				// There are NUM_LEDS_PER_EDGE leds along this edge, but they don't start and end 
				// exactly on the vertices, but are inset a bit. So, we divide the edge into NUM_LEDS_PER_EDGE+2
				// sections as to have a space at either end
				int numDivs = NUM_LEDS_PER_EDGE + 2;
				vec3 edgeVec;
				SubVec3(&edgeVec,&v1,&v0);

				//Serial.println("Edge vector is:");
				//PrintVert(edgeVec);

				float scaleVal = 1.0f/(float)(numDivs);
				//Serial.print("Scale value is: "); Serial.println(scaleVal);

				vec3 divVec;
				ScaleVec3(&divVec,&edgeVec,scaleVal);

				//Serial.println("Div vector is:");
				//PrintVert(divVec);

				// The formula for the Nth led location on this edge is v0 + (N+1)*divVec
				vec3 ledLoc = v0;
				for (int i=0; i<NUM_LEDS_PER_EDGE; i++)
				{
					AddVec3_Self(&ledLoc, &divVec);

					//Serial.println("LED loc is:");
					PrintVert(ledLoc);

					segverts[seg][ledIndex++] = ledLoc;
				}
			}
		}

		Serial.println("LED locations by segment:");
		for (int seg=0; seg<5; seg++)
		{
			Serial.print("Segment "); Serial.print(seg); Serial.println(":");
			for (int led=0; led<NUM_LEDS_PER_EDGE_CHANNEL; led++)
			{
				vec3 v = segverts[seg][led];
				Serial.print(v.x); Serial.print(","); Serial.print(v.y); Serial.print(","); Serial.println(v.z);
			}
		}
		

		Serial.println("================= Dodecahedron Initialize Done =================");

	}

#endif
