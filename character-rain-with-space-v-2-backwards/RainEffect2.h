#ifndef RAINEFFECT2
#define RAINEFFECT2

// The raindrops that we will be managing
#include <arduino.h>
#include "Effect.h"
#include "constants.h"
#include "tunables.h"
#include "timer.h"

// Raineffect owns some "lanes" which correspond to the strands. It spawns raindrops in random lanes. It knows which lanes have active raindrops and always chooses an empty lane in which to spawn
class RainEffect2 : Effect
{
public:

	int lead_char[NUM_PANEL_STRIPS];

	int loop_count = 0;
	uint8_t spawn_strand;

	uint8_t strand_avg_decay[NUM_PANEL_STRIPS];
	uint8_t char_decay;
	int32_t spawn_wait;

	Timer rainDelayTimer;

	Timer spawnTimer;

	void SetPixel(uint8_t strand, uint8_t pixel, CRGB color);

	void FadePixelBy(uint8_t strand, uint8_t pixel, uint8_t fade);

	void Init();

	void Update(unsigned long deltamics);

	void Render();
};


#endif