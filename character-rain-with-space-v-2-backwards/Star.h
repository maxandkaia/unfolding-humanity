#ifndef STAR_H
#define STAR_H

#include "FastLED.h"
#include "Fader.h"

// star states
#define DEAD 0
#define BRIGHTENING 1
#define DIMMING 2
#define FORWARDCOMET 3
#define BACKWARDSCOMET 4

// TO DO: in my mind this should be in StarEffect.h, but I can't #include it here to use in cometbirth below
#define GLITTER 0
#define TWINKLE 1
#define FLASHBULB 2
#define COMET 3
#define COLORGLITTER 4
#define COLORTWINKLE 5
#define RAINBOWFLASHBULB 6
#define COLORCOMET 7
#define BURST 8
#define VERTEX 9

class Star {
	public : 
	    uint8_t state; 
		CHSV color;
		uint8_t edge;
		int pixel;
		Fader starfader;

		Star()
		{
			state = DEAD;
			color = CHSV(255,255,255);
			edge = 0;
			pixel = 0;
		}

		void Init();

		void StarBirth(uint8_t _edge, uint8_t _pixel);

		void CometBirth(uint8_t _edge, uint8_t comettail);

		void StarUpdate(unsigned long deltamics);

		void CometUpdate(unsigned long deltamics, uint8_t comettail);

		// A safe pixel set function, won't index out of bounds
		void SetPixel(uint8_t edge, uint8_t pixel, CRGB color);

		void SetCometPixel(uint8_t edge, uint8_t pixel, CRGB color);
};

#endif