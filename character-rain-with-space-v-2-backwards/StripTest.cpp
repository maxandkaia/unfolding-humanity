
#include "leds.h"
#include "StripTest.h"

	const CRGB StripTest::testColors[8] = {CRGB::Red, CRGB::Green, CRGB::Blue, CRGB::Cyan, CRGB::Magenta, CRGB::Yellow, CRGB::Purple, CRGB::Orange};

	// Run a test
	// phase 1 = identify strip number by color: 0..7 = Red, Green, Blue, Cyan, Magenta, Yellow, Purple, Orange
	// phase 2 = test each strip using a cycle of colors

	void StripTest::SetColorByColumn()
	{
		for (int strip=0; strip<NUM_PANEL_STRIPS; strip++)
		{
			for (int led=0; led<LEDS_PER_PANEL_STRIP; led++)
			{
				leds[strip*LEDS_PER_PANEL_STRIP + led] = testColors[strip];
			}
		}		
	}

	void  StripTest::SetColorByIndex()
	{
		for (int strip=0; strip<NUM_PANEL_STRIPS; strip++)
		{
			for (int led=0; led<LEDS_PER_PANEL_STRIP; led++)
			{
				leds[strip*LEDS_PER_PANEL_STRIP + led] = testColors[curColor];
			}
		}		
	}

	void StripTest::Init()
	{
		testTimer.SetInterval(10.0f);
		testTimer.Start();
		phase = 0;
		done = false;
		curColor = 0;

		SetColorByColumn();
	}

	void SetAllBlack()
	{
		for (int i=0; i<NUM_PANEL_STRIPS*LEDS_PER_PANEL_STRIP; i++)
			leds[i] = CRGB::Black;
	}

	void StripTest::Update(unsigned long deltamics)
	{
		testTimer.Update(deltamics);
		if (testTimer.GetEvent() && !done)
		{
			SetColorByIndex();
			curColor++;
			if (curColor == 8)
			{
				done = true;
			}
		}
	}

	void StripTest::Render()
	{

	}
