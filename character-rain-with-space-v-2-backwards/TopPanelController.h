#ifndef TOP_PANEL_CONTROLLER
#define TOP_PANEL_CONTROLLER
#include <arduino.h>
#include "leds.h"
#include "Controller.h"
#include "raineffect.h"
#include "RainEffect2.h"
#include "BurstEffect.h"
#include "StripTest.h"


class TopPanelController : Controller
{

  	RainEffect raineffect;

  	BurstEffect burstEffect;
  	StripTest stripTest;

  	Timer burstTimer;

  	// A timer to fallback to default mode if neither diane nor max are around
  	Timer presenceTimeoutTimer;

  	// Check every 40 seconds, last only 10 seconds, and only if max or diane are around
  	Timer rainbowRainCheckTimer;
  	Timer rainbowRainDurationTimer;

  	bool maxIsHere;
  	bool dianeIsHere;

  	enum EffectMode
  	{
  		RAINMODE=0,
  		BURSTMODE,
  		TEST
  	};


	public:

		EffectMode mode;

		TopPanelController(uint16_t id, const char* name);

		// Update will process any inputs (sensors, etc), perform comms on the radios, process any commands in the command queue
		void Update(unsigned long deltamics);

    virtual void HandleMessage(const char* message, bool fromSerial);

		// Do any post-construction setup (network initialize etc)
		void Initialize();

		// Render will take the current state of simulation and render it to the led strips (if any)
		void Render();

		// Reset will take us back to the state just after power on (typically only used if something goes wrong)
		void Reset();

};

#endif