
#include "leds.h"
#include "EdgeTest.h"

	const CRGB EdgeTest::testColors[8] = {CRGB::Red, CRGB::Green, CRGB::Blue, CRGB::Cyan, CRGB::Magenta, CRGB::Yellow, CRGB::Purple, CRGB::Orange};

	// Run a test

	// we have two pentagons, the lower (number 0) and the upper (number 1)
	void SetPentagonToColor(int pentagonNumber, CRGB color)
	{
		// Guard against bad memory access!
		if (pentagonNumber != 0 && pentagonNumber != 1) return;

		for (int i=0; i<NUM_EDGES_PER_SEGMENT*NUM_LEDS_PER_EDGE; i++)
			edgeleds[pentagonNumber][i] = color;
	}

	void EdgeTest::Init()
	{
		testTimer.SetInterval(20.0f);
		testTimer.Start();
		phase = 0;
		done = false;
	}


	void EdgeTest::Update(unsigned long deltamics)
	{
		testTimer.Update(deltamics);
		if (testTimer.GetEvent() && !done)
		{
				// <<TODO>> test the inside and outside strips
			phase++;
		}

		if (phase == 0)
		{
			SetPentagonToColor(0, testColors[0]);
			SetPentagonToColor(1, testColors[1]);
		}
		else if (phase == 1)
		{
			SetPentagonToColor(0, testColors[2]);
			SetPentagonToColor(1, testColors[3]);		
		}
	}

	void EdgeTest::Render()
	{

	}
