#ifndef MAXBIKE
#define MAXBIKE

// The raindrops that we will be managing
#include <arduino.h>
#include "Effect.h"
#include "constants.h"
#include "tunables.h"
#include "timer.h"

#define NUM_MAXBIKE_LEDS 264

#define BIKEPULSE 0
#define BIKESCROLL 1
#define BIKESHIFT 2
#define BIKEBURST 3
#define NUM_MAXBIKE_MODES 4


#define MIDDLE1 66
#define MIDDLE2 198

// Raineffect owns some "lanes" which correspond to the strands. It spawns raindrops in random lanes. It knows which lanes have active raindrops and always chooses an empty lane in which to spawn
class MaxBike : Effect
{
	Timer maxBikePulseTimer;
	CRGB maxBikeleds[NUM_MAXBIKE_LEDS];
	
	bool nearStructure;
	uint8_t mode;

	Timer hueShiftTimer;
	Timer patternShiftTimer;
	Timer modeChangeTimer;

	CRGB patternBuffer[32];
	int patternSize = 32;
	int patternCounter = 0;

// Some hues to vary around
	uint8_t hueA, hueB;
	uint8_t satA, satB;
	uint8_t valA, valB;


	// Just some local vars to do a test
	uint8_t curPalOffset = 0;

public:

	// This Init routine takes an argument of "which segment to control"
	void Init();

	void Update(unsigned long deltamics);

	void Render();

	void SetStripesPattern(CRGB* buffer, int patternSize, CRGB c1, CRGB c2, CRGB c3, CRGB c4);
	void ShiftOut();
	void SetMiddle(CRGB color);	
};


#endif