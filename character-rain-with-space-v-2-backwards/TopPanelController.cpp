#define DEBUG
#include "debug_utils.h"

#include "TopPanelController.h"
#include "raineffect.h"
#include "BurstEffect.h"
#include "StripTest.h"

TopPanelController::TopPanelController(uint16_t id, const char* name) : Controller(id, name)
{

  // Only allocate the leds array if we are building a top panel controller
  leds = new CRGB[NUM_PANEL_STRIPS * LEDS_PER_PANEL_STRIP];

  FastLED.addLeds<OCTOWS2811>(leds, LEDS_PER_PANEL_STRIP);

  // set all black
  FastLED.clear(); 

  raineffect.Init();  

  burstEffect.Init();
  stripTest.Init();

  mode = RAINMODE;

  raineffect.topPanel = false;
  if (id == TOP_PANEL_1_A) raineffect.topPanel = true;
  if (id == TOP_PANEL_1_B) raineffect.topPanel = true;
  if (id == TOP_PANEL_2_A) raineffect.topPanel = true;
  if (id == TOP_PANEL_2_B) raineffect.topPanel = true;
  if (id == TOP_PANEL_3_A) raineffect.topPanel = true;
  if (id == TOP_PANEL_3_B) raineffect.topPanel = true;
  if (id == TOP_PANEL_4_A) raineffect.topPanel = true;
  if (id == TOP_PANEL_4_B) raineffect.topPanel = true;
  if (id == TOP_PANEL_5_A) raineffect.topPanel = true;
  if (id == TOP_PANEL_5_B) raineffect.topPanel = true;

}

    // Do any post-construction setup (network initialize etc)
    void TopPanelController::Initialize()
    {
      Controller::Initialize();

      // A burst lasts for 10 seconds;
      burstTimer.SetInterval(10.0f);

      presenceTimeoutTimer.SetInterval(60.0f);
      presenceTimeoutTimer.Start();

      // Rainbow rain lasts for 10 seconds
      rainbowRainCheckTimer.SetInterval(40.0f);
      rainbowRainCheckTimer.Start();
      rainbowRainDurationTimer.SetInterval(10.0f);


    }

// Update will process any inputs (sensors, etc), perform comms on the radios, process any commands in the command queue
void TopPanelController::Update(unsigned long deltamics)
{
  burstTimer.Update(deltamics);
  presenceTimeoutTimer.Update(deltamics);
  rainbowRainCheckTimer.Update(deltamics);
  rainbowRainDurationTimer.Update(deltamics);

  // Burst mode only lasts for some number of seconds, then go back to rainmode
  if (burstTimer.GetEvent())
  {
    mode = RAINMODE;
    raineffect.Init();
    burstTimer.Stop();
  }

  // If this timer ticks, remove diane and max from the game
  // We reset the timer every time we detect presence of either person
  if (presenceTimeoutTimer.GetEvent())
  {
    dianeIsHere = false;
    maxIsHere = false;
  }

	// TODO
  if (mode == RAINMODE)
  {
    // DIANE: Try both of these out A/B test fashion
    // Just comment one or the other out....

    // The new-ish
	  raineffect.Update(deltamics);
  }
  else if (mode == BURSTMODE)
    burstEffect.Update(deltamics);
  else if (mode == TEST)
  {
    stripTest.Update(deltamics);
    if (stripTest.done)
    {
      // Go back to rainmode
      raineffect.Init();
      mode = RAINMODE;
    }
  }

  // If either diane or max are here, set rainbow rain for 10 seconds and then plain rain for 50, but NOT
  // when bursting
  if (rainbowRainCheckTimer.GetEvent() && mode != BURSTMODE)
  {
    if (dianeIsHere || maxIsHere)
    {
      raineffect.rainbowMode = true;
      rainbowRainDurationTimer.Start();
    }
  }

  if (rainbowRainDurationTimer.GetEvent())
  {
    raineffect.rainbowMode = false;
    rainbowRainDurationTimer.Stop();
    rainbowRainDurationTimer.Reset();
  }


    // Update the base class (the comms router gets updated this way)
  Controller::Update(deltamics);


}

void TopPanelController::HandleMessage(const char* message, bool fromSerial)
{
  Serial.println("Handling message:");
  Serial.println(message);

  if (!strcmp(message,"burstmode") && mode != BURSTMODE) 
  {
    // Go into burst mode for 10 seconds
    mode = BURSTMODE;
    burstEffect.Init();
    burstTimer.Reset();
    burstTimer.Start();
  }
  else if (!strcmp(message,"rainmode"))
  {
    mode = RAINMODE;
    raineffect.Init();
  }
  else if (!strcmp(message,"striptest"))
  {
    mode = TEST;
    stripTest.Init();
  }
  else if (!strcmp(message,"add_max"))
  {
    maxIsHere = true;
  }
  else if (!strcmp(message,"remove_max"))
  {
    maxIsHere = false;
  }
  else if (!strcmp(message,"add_diane"))
  {
    dianeIsHere = true;
  }
  else if (!strcmp(message,"remove_diane"))
  {
    dianeIsHere = false;
  }
  else
  {
    Controller::HandleMessage(message, false);
  }
}


// Render will take the current state of simulation and render it to the led strips (if any)
void TopPanelController::Render()
{
	// TODO
	 	FastLED.show();
}

// Reset will take us back to the state just after power on (typically only used if something goes wrong)
void TopPanelController::Reset()
{
	Controller::Reset();
	// set all black
  	FastLED.clear(); 
}
