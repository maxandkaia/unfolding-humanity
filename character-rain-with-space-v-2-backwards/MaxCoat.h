#ifndef MAXCOAT
#define MAXCOAT

// The raindrops that we will be managing
#include <arduino.h>
#include "Effect.h"
#include "constants.h"
#include "tunables.h"
#include "timer.h"

#define NUM_MAXCOAT_LEDS 141

// Raineffect owns some "lanes" which correspond to the strands. It spawns raindrops in random lanes. It knows which lanes have active raindrops and always chooses an empty lane in which to spawn
class MaxCoat : Effect
{

#define PATTERN 0
#define SCROLL 1
#define NUM_MAXCOAT_MODES 2


	CRGB maxleds[NUM_MAXCOAT_LEDS];

	uint8_t curPalOffset = 0;
	bool nearStructure;
	uint8_t mode;

	Timer hueShiftTimer;
	Timer patternShiftTimer;
	Timer modeChangeTimer;

	CRGB patternBuffer[32];
	int patternSize = 32;
	int patternCounter = 0;

// Some hues to vary around
	uint8_t hueA, hueB;
	uint8_t satA, satB;
	uint8_t valA, valB;
	
public:

	// This Init routine takes an argument of "which segment to control"
	void Init();

	void Update(unsigned long deltamics);

	void Render();

		// Some pattern definition utilities
	void SetStripesPattern(CRGB* buffer, int patternSize, CRGB c1, CRGB c2, CRGB c3, CRGB c4);

	void ShiftOut();
	void SetMiddle(CRGB color);

	void ArriveStructure();
	void LeaveStructure();
};


#endif