#include "Pulse.h"
// Pulse class creates a one-shot waveform that evolves over the specified time
// Value at any time can be queried, and is returned as a value between min and max
// As specified at trigger time.


  void Pulse::Start(PulseType _type, float _duration, float _min, float _max)
  {
  	type = _type;
  	min = _min;
  	max = _max;
  	micsperpulse = (unsigned long)(_duration * 1000000.0f);
  	micselapsed = 0;
  	running = true;
  }

  void Pulse::Update(unsigned long mics)
  {
  	// Avoid work
  	if (!running)
  		return;

  	// Add mics
  	micselapsed += mics;

  	// Check for done
  	if (micselapsed >= micsperpulse)
  	{
  		micselapsed = micsperpulse;
  		running = false;
  		return;
  	}
  }

  bool Pulse::Running()
  {
  	return running;
  }

  float Pulse::GetValue()
  {
  	if (!running)
  		return 0.0f;

  	// Compute parameter T (0.0 to 1.0)
  	float T = (float)micselapsed/(float)(micsperpulse);

  	// Create basic SINE half-waveform

  	// Change T to radians
  	T *= PI;
  	float s;

  	if (type == SINE)
  	{
		s = sin(T); // half wave should go from 0 to 1 and back to 0
	}
	else if (type == SPIKE)
	{
		s = 1.0f - abs(cos(T)); // half wave inverted and shifted

	}
	else if (type == SAW)
	{
		if (T < 0.5f)
		{
			s = T*2.0f;
		}
		else
		{
			s = (1.0f-T)*2.0f;
		}
	}


  	return min + (s*(max-min));

  }
  
