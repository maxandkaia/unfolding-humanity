#include "leds.h"
#include "constants.h"
#include "tunables.h"
#include "number_utils.h"
#include "Star.h"
#include "Fader.h"


        // A safe pixel set function, won't index out of bounds
void inline Star::SetPixel(uint8_t edge, uint8_t pixel, CRGB color)
{
    if (edge < 4 || edge > 7) return;
    if (pixel < 0 || pixel > 126) return;
    edgeleds[edge][pixel] = color;
}
        // A safe pixel set function, won't index out of bounds
void inline Star::SetCometPixel(uint8_t edge, uint8_t pixel, CRGB color)
{
	int clamp = 24;
    if (edge < 4 || edge > 7) return;
    if (pixel < 0 || pixel > 126) return;

    edgeleds[edge][pixel] /= 2;
    color /=2;
    color += edgeleds[edge][pixel];
    if (color.r < clamp && color.g < clamp && color.b < clamp)
    {
    	color = CRGB::Black;
	}

    edgeleds[edge][pixel] = color;

}



// to do: this should really refer to stareffect.h 

void Star::Init() {
	state = DEAD;
}

void Star::StarBirth(uint8_t _edge, uint8_t _pixel) {
	state = BRIGHTENING;

	if (TunablesManager::starmode == TWINKLE) {
		uint8_t chosencolor = random8(10);
		switch(chosencolor) {
			case 0:
				color = TunablesManager::whitestarcolors[0];
				break;
			case 1:
			case 2:
				color = TunablesManager::whitestarcolors[1];
				break;
			case 3:
			case 4:
			case 5:
			case 6:
			 	color = TunablesManager::whitestarcolors[2];
			 	break;
			case 7:
			case 8:
				color = TunablesManager::whitestarcolors[3];
				break;
			case 9:
				color = TunablesManager::whitestarcolors[4];
				break;
			default:
				color = TunablesManager::whitestarcolors[2];
			 	break;
		}
	}
	else { // TunablesManager::starmode == COLORTWINKLE 
		uint8_t hue = random8(0,255);
		color = CHSV(hue, 100, 128);
	}
	edge = _edge;
	pixel = _pixel;

	// set star to fade in
	float fadeintime = utils::frand(TunablesManager::minstarfadein, TunablesManager::maxstarfadein);
	starfader.Init(0.0f, 255.0f, fadeintime);
	CHSV fadedcolor = color;
	fadedcolor.v = (uint8_t) starfader.GetValue();
	edgeleds[edge][pixel] = fadedcolor;
}

void Star::CometBirth(uint8_t _edge, uint8_t comettail) {
	edge = _edge;

	//Serial.print("A comet is born at edge, pixel: ");
	//Serial.print(edge);
	//Serial.println(pixel);

	// a comet is state 3 or 4
	state = random8(0,2) + 3;

	// only forward for now - remove later
//	state = BACKWARDSCOMET;
//	state = FORWARDCOMET;
	if (state == FORWARDCOMET) {
		pixel = 0;
	}
	else {
		pixel = 42*3; // change to 210
	}

	if (TunablesManager::starmode == COMET) {
		color = CHSV(0,0,255);
	}
	else { // choose color for color comet
		uint8_t chosencolor = random8(6);
		switch(chosencolor) {
			case 0:
				color = CHSV(0,255,255);
				break;
			case 1:
				color = CHSV(85,255,255);
				break;
			case 2:
				color = CHSV(171,255,255);
				break;
			case 3:
				color = CHSV(42,255,255);
				break;
			case 4:
				color = CHSV(127,255,255);
				break;
			case 5:
				color = CHSV(211,255,255);
				break;
			default:
				color = TunablesManager::whitestarcolors[2];
 				break;
		}
	}

	edgeleds[edge][pixel] = color;
}

void Star::StarUpdate(unsigned long deltamics) {

	if (state == DEAD) return;

	starfader.Update(deltamics);
	CHSV fadedcolor = color;
	fadedcolor.v = (uint8_t) starfader.GetValue();
	edgeleds[edge][pixel] = fadedcolor;

	if (starfader.IsFinished()) 
	{
		if (state == BRIGHTENING) {
			// set star to fade out
			state = DIMMING;
			float fadeouttime = utils::frand(TunablesManager::minstarfadeout, TunablesManager::maxstarfadeout);
    		starfader.Init(255.0f, 0.0f, fadeouttime);
    	}
    	else { // has already brightened and dimmed; time to die
    		state = DEAD;
    		edgeleds[edge][pixel] = CRGB::Black;
    	}
	}
}
	
void Star::CometUpdate(unsigned long deltamics, uint8_t comettail) {

	int visible_threshold = 100;

	if (state == DEAD) {
		return;
	}
	else if ((state == BRIGHTENING) || (state == DIMMING)) {
		// turn an old star into a comet and give it a direction
		state = random8(0,2) + 3;
	}
	else if (state == FORWARDCOMET) {
		if (pixel <= 126+comettail) {
			SetCometPixel(edge, pixel, color);
			CHSV rampedColor = color;
			uint8_t vdelta = rampedColor.v / comettail;
			// I'm not includeing above line in loop below because fadeToBlackBy needs CRGB rather than CHSV
			// maybe there's a tidier way
			for (uint8_t i = 1; i < comettail; i++) {
				rampedColor.v -= vdelta;
				//SetCometPixel(edge, pixel-i, edgeleds[edge][pixel].fadeToBlackBy(i*256/comettail));
				SetCometPixel(edge, pixel-i, rampedColor);
			}
			SetCometPixel(edge, pixel-comettail, CRGB::Black);
			pixel++;
		}
		else {
			state = DEAD;
		}
	} 
	else if (state == BACKWARDSCOMET) {

		if (pixel+comettail >= 0) {
			SetCometPixel(edge, pixel, color);
			CHSV rampedColor = color;
			uint8_t vdelta = rampedColor.v / comettail;
			for (uint8_t i = 0; i < comettail; i++) {
				rampedColor.v -= vdelta;
			//	SetCometPixel(edge, pixel+i, edgeleds[edge][pixel].fadeToBlackBy(i*256/comettail));
				SetCometPixel(edge, pixel+i, rampedColor);
			}
			SetCometPixel(edge, pixel+comettail, CRGB::Black);
			pixel--;
		}
		else {
			state = DEAD;
			//edgeleds[edge][pixel] = CRGB::Black;
		}
	}
}

