#include "LFO.h"
#include "FastLED.h"

// Timer class just counts down to "event"
// When timer hits 0, event flag is set
// Event flag is only unset by fetching it, or by Reset
// Timer can either roll over and continue a new countdouwn, or just stop, depending on mode


  LFO::LFO(LFOMode _mode, int _min, int _max, int _bpm)
  {
  	mode = _mode;
  	min = MIN(_min, _max);
  	max = MAX(_min, _max);
  	bpm = _bpm;
  	range = max - min;
  }

  int LFO::Get()
  {
  	int cycle;
  	if (mode == SINE)
  		cycle = beatsin16(bpm<<8);
  	else
  		cycle = beat16(bpm<<8);

    if (mode == PEAKS)
    {
      if (cycle > 32768)
      {
        cycle = 32768 - cycle;
      }
      cycle *= 2;
    }

  	// Now map cycle to user range (0..65535 to min..max)
  	long val = min + (range * cycle);
  	return val>>8;

  }
