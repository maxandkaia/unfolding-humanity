

#include "leds.h"
#include "RainEffect2.h"


// color constants
#define LEAD_SAT 190 // saturations that still look green to me range from 160-255
#define LEAD_VAL 255
#define TRAIL_SAT 255
#define MAX_TRAIL_GREEN_VAL 128

// animation constants
#define MIN_DECAY 13 // 13/256 is about 5%
#define MAX_DECAY 77 // 77/256 is about 30%
#define DECAY_RANGE 5 // must be less than MIN_DECAY
#define AVG_DECAY_STEPS 12
#define MIN_SPAWN_WAIT 20 // 2
#define MAX_SPAWN_WAIT 50 // 15

// This is 75 milliseconds, but using delay(X) is wasteful, so we will use a timer
#define RAIN_DELAY 75

#define IS_UNAVAILABLE(strand) (lead_char[strand] >  0 - AVG_DECAY_STEPS )



//--------------------------- SETUP() ---------------------------------

void RainEffect2::Init() { 

  for (uint8_t strand = 0; strand < NUM_PANEL_STRIPS; strand++) {
    lead_char[strand] = 0 - AVG_DECAY_STEPS;
  }

// This timer takes the place of "delay(RAIN_DELAY)" and let's the processor continue doing useful stuff
  rainDelayTimer.SetInterval(0.075f);
  rainDelayTimer.Start();

  rainDelayTimer.SetInterval(0.075f);
  rainDelayTimer.Start();

  spawn_wait = random(MIN_SPAWN_WAIT, MAX_SPAWN_WAIT + 1) * RAIN_DELAY;

  Serial.print("spawn_wait is: ");Serial.println(spawn_wait);
  Serial.print("loop_count is: "); Serial.println(loop_count);
}

void RainEffect2::SetPixel(uint8_t strand, uint8_t pixel, CRGB color)
{
  if (strand < 0 || strand >= NUM_PANEL_STRIPS) return;
  if (pixel < 0 || pixel >= LEDS_PER_PANEL_STRIP) return;

  leds[strand*LEDS_PER_PANEL_STRIP + pixel] = color;
}

void RainEffect2::FadePixelBy(uint8_t strand, uint8_t pixel, uint8_t fade)
{
  if (strand < 0 || strand >= NUM_PANEL_STRIPS) return;
  if (pixel < 0 || pixel >= LEDS_PER_PANEL_STRIP) return;

  leds[strand*LEDS_PER_PANEL_STRIP + pixel].fadeToBlackBy(fade);  
}

//--------------------------- LOOP() ---------------------------------

void RainEffect2::Update(unsigned long deltamics) { 

  rainDelayTimer.Update(deltamics);
  if (rainDelayTimer.GetEvent())
  {
 // Serial.print("spawn_wait is: ");Serial.println(spawn_wait);
  //Serial.print("loop_count is: "); Serial.println(loop_count);
    // check if time to spawn new raindrop
    if (loop_count % spawn_wait == 0) {

      // choose *unlit* strand to spawn. if none, then don't spawn
      spawn_strand = random(0, NUM_PANEL_STRIPS);

      // if we didn't check all strands without finding available one, then spawn new raindrop
  //    if (NUM_PANEL_STRIPS_checked < NUM_PANEL_STRIPS) {

      Serial.print("spawn_strand is: "); Serial.println(spawn_strand);
      Serial.print("lead_char[spawn_strand] is:"); Serial.println(lead_char[spawn_strand]);

       if (lead_char[spawn_strand] < 0 - AVG_DECAY_STEPS) {    

        Serial.println("Setting lead character");

        lead_char[spawn_strand] = CHARS_PER_PANEL_STRIP - 1;
      
        // re-randomize spawn wait and  decay
        spawn_wait = random(MIN_SPAWN_WAIT, MAX_SPAWN_WAIT + 1)  * RAIN_DELAY;
        strand_avg_decay[spawn_strand] = random(MIN_DECAY, MAX_DECAY + 1);
      }
    }
      

    // now move lead character or decay existing green
    for(uint8_t strand = 0; strand < NUM_PANEL_STRIPS; strand++)
    {
      for (uint8_t character = 0; character < CHARS_PER_PANEL_STRIP; character++)
      {

        //sSerial.println("Moving character");

        // if at lead character, color character MAX_GREEN_BRIGHTNESS
        if (lead_char[strand] == character)
        {
          for (uint8_t pixel = 0; pixel < PIXELS_PER_CHAR- 1; pixel++)
          {
            //leds[strand][character * PIXELS_PER_CHAR + pixel] = CHSV(HUE_GREEN, LEAD_SAT, LEAD_VAL);

            Serial.println("Setting pixel to green");

            SetPixel(strand, character * PIXELS_PER_CHAR + pixel, CHSV(HUE_GREEN, LEAD_SAT, LEAD_VAL));
          }
        }

        // if at character trailing lead character, full green sat, big step down in brightness
        else if (lead_char[strand] + 1 == character)
        {
          for (uint8_t pixel = 0; pixel < PIXELS_PER_CHAR - 1; pixel++)
         {
            //leds[strand][character * PIXELS_PER_CHAR + pixel] = CHSV(HUE_GREEN, TRAIL_SAT, MAX_TRAIL_GREEN_VAL);
            SetPixel(strand, character * PIXELS_PER_CHAR + pixel, CHSV(HUE_GREEN, TRAIL_SAT, MAX_TRAIL_GREEN_VAL));
          }
        }

        
        // else decay the current green
        else
       {
          // choose decay amount - don't have every character fade at same, or even linear, rate
          char_decay = random(strand_avg_decay[strand] - DECAY_RANGE, strand_avg_decay[strand] + DECAY_RANGE + 1);

          // decay the brightness on the green
          for (uint8_t pixel = 0; pixel < PIXELS_PER_CHAR - 1; pixel++)
          {
              //leds[strand][character * PIXELS_PER_CHAR + pixel].fadeToBlackBy(char_decay);
              FadePixelBy(strand, character * PIXELS_PER_CHAR + pixel, char_decay);
          }
        }
      }
      lead_char[strand]--;
    }

// The show() call is done in the controller
  //FastLED.show();

  // The delay is accomplised by a timer
  //delay(RAIN_DELAY);
  loop_count++;

  }


}

void RainEffect2::Render()
{
  
}