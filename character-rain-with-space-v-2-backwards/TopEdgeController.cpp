#define DEBUG
#include "debug_utils.h"
#include "TopEdgeController.h"
#include "timer.h"
#include "star.h"

// To save off the nominal value so we can return to it
float nominalGlitterDensity;

TopEdgeController::TopEdgeController(uint16_t id, const char* name) : Controller(id, name)
{

	FastLED.addLeds<OCTOWS2811>(&edgeleds[0][0], NUM_LEDS_PER_EDGE_CHANNEL);

	// set all black
	FastLED.clear(); 

	TunablesManager::starmode = COLORCOMET;

	edgeTest.Init();	
	edgeEffect.Init();
	starEffect.Init();

	// The top edge controllers have ids 1..5, so the segments they control are (id-1) respectively
	mySegment = id - 1;

	edgeCrawl.Init(mySegment);

	maxmodeTimer.SetInterval(30.0f);
	maxmodeTimer.Start();

	nominalGlitterDensity = TunablesManager::glitterdensity;

	// bursts last 10 seconds
	burstmodeTimer.SetInterval(10.0f);
}

// Do any post-construction setup (network initialize etc)
void TopEdgeController::Initialize()
{
	Controller::Initialize();
}

// These control the length of time each of the modes will play...
float glitterDuration = 20.0f;
float twinkleDuration = 20.0f;
float flahsbulbDuration = 20.0f;
float cometDuration = 20.0f;
float colorcometDuration = 20.0f;


// Update will process any inputs (sensors, etc), perform comms on the radios, process any commands in the command queue
void TopEdgeController::Update(unsigned long deltamics)
{
//		 	starEffect.ClearAllEdges();

	//Serial.println("top edge controller update");
	maxmodeTimer.Update(deltamics);
	burstmodeTimer.Update(deltamics);
	
	// For now, just a test mode. Later add more effects/modes
	//edgeTest.Update(deltamics);
	edgeEffect.Update(deltamics);

	starEffect.Update(deltamics);
	//Serial.println("in top edge controller done with star effect update");

	glitterPulse.Update(deltamics);

	if (maxmodeTimer.GetEvent())
	{
		edgeEffect.mode = NORMALMODE;
	}

	// At the end of a burst, go back to normal mode
	if (burstmodeTimer.GetEvent())
	{
		edgeEffect.mode = NORMALMODE;
		burstmodeTimer.Stop();
		burstmodeTimer.Reset();
		starEffect.ResetAllStars();
		starEffect.ClearAllEdges();
		TunablesManager::starmode = COMET;
	}

	if (glitterPulse.Running())
	{
		TunablesManager::glitterdensity = nominalGlitterDensity + (int)glitterPulse.GetValue();
	}
	else
	{
		TunablesManager::glitterdensity = nominalGlitterDensity;
	}

	// Update the base class (the comms router gets updated this way)
	Controller::Update(deltamics);

	//Serial.println("in top edge controller done with controller update");

}

void TopEdgeController::HandleMessage(const char* message, bool fromSerial)
{
	if (!strcmp(message,"glitter") && TunablesManager::starmode != GLITTER)
	{
		Serial.println("Glitter star mode");
		starEffect.ResetAllStars();
		starEffect.ClearAllEdges();
		TunablesManager::starmode = GLITTER;
	}
	else if (!strcmp(message,"twinkle") && TunablesManager::starmode != TWINKLE)
	{
		Serial.println("Twinkle star mode");
		starEffect.ResetAllStars();
		//starEffect.ClearAllEdges();
		TunablesManager::starmode = TWINKLE;
	}
	else if (!strcmp(message,"flashbulb") && TunablesManager::starmode != FLASHBULB)
	{
		Serial.println("Flashbulb star mode");
		starEffect.ResetAllStars();
		starEffect.ClearAllEdges();
		TunablesManager::starmode = FLASHBULB;
	}
	else if (!strcmp(message,"comet") && TunablesManager::starmode != COMET)
	{
		Serial.println("Comet star mode");
		starEffect.ResetAllStars();
		// starEffect.ClearAllEdges();
		TunablesManager::starmode = COMET;
	}
	else if (!strcmp(message,"colorglitter") && TunablesManager::starmode != COLORGLITTER)
	{
		Serial.println("Color Glitter star mode");
		starEffect.ResetAllStars();
		starEffect.ClearAllEdges();
		TunablesManager::TunablesManager::starmode = COLORGLITTER;
	}
	else if (!strcmp(message,"colortwinkle") && TunablesManager::starmode != COLORTWINKLE)
	{
		Serial.println("Color Twinkle star mode");
		starEffect.ResetAllStars();
		//starEffect.ClearAllEdges();
		TunablesManager::starmode = COLORTWINKLE;
	}
	else if (!strcmp(message,"colorcomet") && TunablesManager::starmode != COLORCOMET)
	{
		Serial.println("Color Comet star mode");
		starEffect.ResetAllStars();
		//starEffect.ClearAllEdges();
		TunablesManager::starmode = COLORCOMET;
	}
	else if (!strcmp(message,"rainbowflashbulb") && TunablesManager::starmode != RAINBOWFLASHBULB)
	{
		Serial.println("Rainbow Flashbulb star mode");
		starEffect.ResetAllStars();
		starEffect.ClearAllEdges();
		TunablesManager::starmode = RAINBOWFLASHBULB;
	}
	else if (!strcmp(message,"maxmode"))
	{
		Serial.println("max mode for outside edges");
		edgeEffect.mode = MAXMODE;
		maxmodeTimer.Reset();
	}
	else if (!strcmp(message,"burstmode"))
	{
		Serial.println("burstmode for outer edges");
		edgeEffect.mode = EDGEBURST;
		edgeEffect.SetRainbowPattern();
		TunablesManager::starmode = BURST;
		burstmodeTimer.Start();
	}
	else if (!strcmp(message,"glitterspike"))
	{
		Serial.println("glitterspike!");
		// If we are in glitter or colorglitter modes, and we get the spike message, DO IT!
		if ((TunablesManager::starmode == GLITTER) || (TunablesManager::starmode == COLORGLITTER))
			glitterPulse.Start(PulseType::SPIKE, 5.0f, 0.0f, 40.0f);
	}
	else // Send any unrecognized or generic messages to the base class for possible processing
		Controller::HandleMessage(message, false);

}


// Render will take the current state of simulation and render it to the led strips (if any)
void TopEdgeController::Render()
{
	 	FastLED.show();

}

// Reset will take us back to the state just after power on (typically only used if something goes wrong)
void TopEdgeController::Reset()
{
	Controller::Reset();
}
