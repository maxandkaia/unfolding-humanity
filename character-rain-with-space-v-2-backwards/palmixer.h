#ifndef MIXER_H
#define MIXER_H

#include "FastLed.h"

// In this class we animate some faders or increments
// and then support mixing of the two renderbuffers into the frontbuffer
// Which then gets replicated (or not) and displayed

class palmixer
{
  public:

  static float fader; // Goes from 0.0f to 1.0f
  static float deltaFade; // amount to fade per second
  static bool active; // are we currently animating anything?
  static fract8 fraction;
  

  enum PalChoice {
  kClouds = 0,
  kRainbow,
  kParty,
  kPurpleFly,
  kIris,
  kMadras,
  kLailah,
  kHope,
  kCompassion,
  kRepose,
  kMermaid,
  kSerendil,
  kFlame,
  kDaisyFae,
  kFaeCat,
  kFireIce,
  kHangOn,
  kMistress,
  kOtis,
  kScoutie,
  kSunlitWave,
  kTrove,
  kBlackhorse,
  kPlumbago,
  kSchwarzwald,
  kNrwc,
  kEpochs,
  kSky04,
  kSky12,
  kSky34,
  kNumPalettes
};


// An array of palette pointers so we can randomly choose one
//#define kNumPalettes 64
static CRGBPalette16 palettes[kNumPalettes];

// when we want to blend to a new palette, we assign to nextPalette, then use nblend to modify the target palette over time.
// all color fetch functions use the final palette in some way or another
static CRGBPalette256 nextPalette;
static CRGBPalette256 curPalette;
static CRGBPalette256 finalPalette;

// This generates in mem copies of all of the progmem palettes that we actually want
// These get upscaled into 256 entry palettes when assigned to nextPalette
static void GenerateGlobalPalettes();
  
static void Update(float dt);

static void SetNewPalette(uint8_t newPal, float seconds);

};

#endif
