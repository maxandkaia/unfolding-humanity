#ifndef PULSE_H
#define PULSE_H
#include <arduino.h>
// Each pulse is a full cycle from min to max and back (they can be inverted)
enum PulseType {
  SINE = 0,
  SAW = 1,
  SPIKE = 2
};

class Pulse
{

// Pulse class creates a one-shot waveform that evolves over the specified time
// Value at any time can be queried, and is returned as a value between min and max
// As specified at trigger time.



unsigned long micsperpulse = 1000000; // one per second by default
long micselapsed = 0;
float min;
float max;
bool running;
PulseType type = SINE;

public:
  
  

  void Start(PulseType type, float duration, float min, float max);

  void Update(unsigned long mics);

  bool Running();

  float GetValue();
  
};
#endif
