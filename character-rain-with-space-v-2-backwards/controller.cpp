#define DEBUG
#include "debug_utils.h"
#include "Controller.h"
#include "Constants.h"
#include "number_utils.h"

// Seconds till comms timeout failsafe
#define COMMS_TIMEOUT 30


		Controller::Controller(uint16_t id , const char* name)
		{
			my_address = id;
			my_name = name;
			router = new CommsRouter(id, this);

			heartbeatTimer.SetFrequency(2.3f);
			heartbeatTimer.Start();

			commsTimeoutTimer.SetInterval(30.0f);
			commsTimeoutTimer.Start();

			messageRetryTimer.SetInterval(1.0f);
      		messageRetryTimer.Start();
      		messageRetryCount = 0;

			edgemessageRetryTimer.SetInterval(1.0f);
      		edgemessageRetryTimer.Start();
      		edgemessageRetryCount = 0;

		}

		void Controller::Initialize()
		{
			router->Initialize();

			commsBlackoutMode = false;

			// Randomize the interval for HB send to avoid network congestion
			heartbeatTimer.SetInterval(2.0f + utils::frand(0.0f, 3.0f));
		}

				// Update will process any inputs (sensors, etc), perform comms on the radios, process any commands in the command queue
		void Controller::Update(unsigned long deltamics)
		{
			// The master controller doesn't need to worry about comms blackout mode, only the children
			if (my_address != 00)			
				commsTimeoutTimer.Update(deltamics);

			//Serial.println("About to update router...");
			
			router->Update(deltamics);
			
			//Serial.println("Done updating router");

			// This is the timer that governs how often we report to the master
			heartbeatTimer.Update(deltamics);
			if (heartbeatTimer.GetEvent())
			{
				char hbmessage[12];
				sprintf(hbmessage,"HB %d", my_address);
				// Only send HB to master if we are not master ourselves
				if (my_address != 00)
					router->SendMessageToMaster(hbmessage);

				// Re-randomize the HB interval to further reduce collision on the airwaves
				heartbeatTimer.SetInterval(2.0f + utils::frand(0.0f, 3.0f));
			}

			// Check to see if comms have gone down (we should get MHB every second. if two seconds goes by, comms are down), and if so, take action
			if (commsTimeoutTimer.GetEvent() && !commsBlackoutMode)
			{
				Serial.println("Comms are down! Taking action...");
				commsBlackoutMode = true;
				// <<TODO>> call some kind of method to kick us back to default behavior
			}

			messageRetryTimer.Update(deltamics);
			if (messageRetryTimer.GetEvent())
			{
				if (messageRetryCount > 0)
				{
					Serial.println("retrying message send...");
					messageRetryCount--;
					router->MulticastToAllPanels(lastmessage);
				}
			}

			// Edge message retry
			edgemessageRetryTimer.Update(deltamics);
			if (edgemessageRetryTimer.GetEvent())
			{
				if (edgemessageRetryCount > 0)
				{
					Serial.println("retrying edge message send...");
					edgemessageRetryCount--;
					router->SendMessageToAllTopEdges(lastedgemessage);
				}
			}
		}

		// Render will take the current state of simulation and render it to the led strips (if any)
		void Controller::Render()
		{

		}

		void Controller::HandleMessage(const char *message, bool fromSerial)
		{
			// Here we handle any generic controller messages. Any message not handled by the subclass should get sent here for handling
//			Serial.println("Base class Controller handling message:");
//			Serial.println(message);

			// "Heartbeat" is handled by all controllers in a generic way. If comms go down, we need to revert to some reasonable default behavior
			if (!strcmp(message, "MHB"))
			{
				commsTimeoutTimer.Reset();
				if (commsBlackoutMode)
				{
					Serial.println("Comms coming back online...");
					commsBlackoutMode = false;
				}
				return;
			}
			else if (!strcmp(message,"announce"))
			{
				Serial.println(my_name);
			}
			else if (!strcmp(message,"help"))
			{
				TunablesManager::PrintHelp();
			}
			else
			{
				// If we don't recognize the message by this point, it may be a tunable param set, so send it on to the tunables manager for parsing
				bool ret = TunablesManager::HandleMessage(message);

				// Successfully processed tunable commands get sent by master to everyone
				if (ret && my_address == MASTER_ID)
				{
					Serial.println("tunable param set message received by MASTER, passing onward");
					// Send this tunable param set message on to ALL controllers, as we want to change the parameter GLOBALLY
					//router->MulticastToAllPanels(message);
					MessageRetry(message, 3);
				}
				// Also, anything entered into the serial commandline to the master get routed onward
				else if (fromSerial && my_address == MASTER_ID)
				{
					Serial.println("serial input message received by MASTER, but not a tunable. Ignoring...");
					//router->MulticastToAllPanels(message);
					//MessageRetry(message, 3);
				}
				else if (!ret)
				{
					Serial.println("Failed to set tunable parameter. Check name and allowed value range?");
				}
			}
		}


		// Reset will take us back to the state just after power on (typically only used if something goes wrong)
		void Controller::Reset()
		{
		}

		void Controller::Announce()
		{
			Serial.println(my_name);
		}


		void Controller::MessageRetry(const char* message, int retrycount)
		{
			messageRetryCount = retrycount;
			strncpy(lastmessage,message,64);
		}


		void Controller::EdgeMessageRetry(const char* message, int retrycount)
		{
			edgemessageRetryCount = retrycount;
			strncpy(lastedgemessage,message,64);
		}

