#ifndef CONTROLLER
#define CONTROLLER
#include <arduino.h>
#include "CommsRouter.h"
#include "MessageHandler.h"
#include "Tunables.h"
#include "Timer.h"


class Controller : public MessageHandler
{
public:
	uint16_t my_address;
	const char* my_name;

	Timer heartbeatTimer;

	Timer commsTimeoutTimer;


	Timer messageRetryTimer;
	char lastmessage[64];
	int messageRetryCount;

	// A separate retry util for the edges. These have to be SUPER reliable. Use high retry count and make the responses
	// IDEMPOTENT
	Timer edgemessageRetryTimer;
	char lastedgemessage[64];
	int edgemessageRetryCount;

	public:
		
		// making this public so that subclasses can respond to changes
		bool commsBlackoutMode;

	// Base class for a controller. Must be implemented by a specialized subclass!
  		CommsRouter* router;


		Controller(uint16_t id, const char* name);

		// Do any post-construction setup (network initialize etc)
		virtual void Initialize();

		// Update will process any inputs (sensors, etc), perform comms on the radios, process any commands in the command queue
		virtual void Update(unsigned long deltamicros);

		virtual void HandleMessage(const char* message, bool fromSerial);

		// Render will take the current state of simulation and render it to the led strips (if any)
		virtual void Render();

		// Reset will take us back to the state just after power on (typically only used if something goes wrong)
		virtual void Reset();

		// Print out my name for debugging
		virtual void Announce();

		void MessageRetry(const char* message, int retrycount);

		void EdgeMessageRetry(const char* message, int retrycount);

};

#endif