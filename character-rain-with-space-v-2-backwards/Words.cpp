#include <arduino.h>
#include "Words.h"
#include "Constants.h"


	CharPos Words::characterPositions[MAX_CHARS];
	uint8_t Words::numCharacters = 0;

	void Words::AddCharacter(uint8_t stripNum, uint8_t charNum)
	{
		if (numCharacters >= MAX_CHARS) return;
		characterPositions[numCharacters].strip = stripNum;
		characterPositions[numCharacters].character = charNum;
		numCharacters++;
	}


	// This routine sets up the character data
	void Words::Init(uint16_t controllerID)
	{
		switch (controllerID)
		{
			case TOP_PANEL_1_A:
			AddCharacter(0,2);
			AddCharacter(1,4);
			AddCharacter(2,7);
			break;

			default:
			break;
		}
	}

	// Check to see if a particular character is in any of the special locations
	bool Words::IsInWord(uint8_t stripNum, uint8_t charNum)
	{
		for (int i=0; i<numCharacters; i++)
		{
			if (characterPositions[i].strip == stripNum && characterPositions[i].character == charNum)
				return true;
		}
		return false;
	}
