
#include "leds.h"
#include "EdgeCrawl.h"

// An effect that makes a pattern "crawl" along the edges. In order to make it look nice even when the segments are not
// synchronized with one another, and to make it look nice even though the edges are addressed in a non-circuit way
// we will have the pattern emerge from every edge's center and move toward the vertices
	void EdgeCrawl::Init(int segment)
	{
		mySegment = segment;
	}

	// <<TODO>> Make color tunable
	// <<TODO>> Make speed tunable
	// <<TODO>> Make repeat count tunable

	void EdgeCrawl::Update(unsigned long deltamics)
	{
		/*
		// outside edge leds start at index NUM_LEDS_PER_EDGE_CHANNEL and proceed from there
		int firstCenterLed = NUM_LEDS_PER_EDGE_CHANNEL + NUM_LEDS_PER_EDGE/2 - 1;
		int count = NUM_LEDS_PER_EDGE/2;
		
		// For each edge in this segment, calculate a 0..255 value that oscillates over time and use it to create a
		// wrapping 0.255 index for each led of the edge
		uint8_t rollingIndex = beat16(120)%256;

		// Start with first edge-center and go outward, then proceed to next edge etc.
		for (int i=0; i<count; i++)
		{
			for (int e=0; e<NUM_EDGES_PER_SEGMENT; e++)
			{
				edgeleds[(NUM_LEDS_PER_EDGE*e) + firstCenterLed - i] = CRGB(0,rollingIndex,0);
				edgeleds[(NUM_LEDS_PER_EDGE*e) + firstCenterLed + i + 1] = CRGB(0,rollingIndex,0);

			}

			// <<TODO>> Make this tunable
			rollingIndex += 24;
		}
		*/
	}

	void EdgeCrawl::Render()
	{

	}
