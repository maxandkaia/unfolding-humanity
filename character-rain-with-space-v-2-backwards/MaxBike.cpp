#define FASTLED_FORCE_SOFTWARE_SPI
#include "leds.h"
#include "MaxBike.h"
#include "palmixer.h"

CRGB RealPink = CRGB(255, 25, 25);

	void MaxBike::Init()
	{
		FastLED.addLeds<APA102, 7, 14, BGR, DATA_RATE_MHZ(4)>(maxBikeleds, NUM_MAXBIKE_LEDS);
		FastLED.setBrightness(128);

		maxBikePulseTimer.SetFrequency(0.25f);
		maxBikePulseTimer.Start();

		modeChangeTimer.SetInterval(30.0f);
		modeChangeTimer.Start();

		hueShiftTimer.SetInterval(5.0f);
		hueShiftTimer.Start();

		patternShiftTimer.SetFrequency(40.0f);
		patternShiftTimer.Start();

		pinMode(0,INPUT);
		pinMode(1,INPUT);

		mode = BIKESCROLL;

	}


	void MaxBike::Update(unsigned long deltamics)
	{
		bool lowPowerMode = (digitalRead(0)==HIGH);

		maxBikePulseTimer.Update(deltamics);
		patternShiftTimer.Update(deltamics);
		hueShiftTimer.Update(deltamics);
		modeChangeTimer.Update(deltamics);

		// change modes sometimes
		if (modeChangeTimer.GetEvent())
		{
			if (!nearStructure)
			{
				mode++;
				if (mode == NUM_MAXBIKE_MODES)
					mode = 0;
			}	
		} 		

		if (lowPowerMode)
		{
			if (maxBikePulseTimer.GetEvent())
			{
				// Set the first 12 pixels to pink
				for (int i=0; i<12; i++)
				{
					maxBikeleds[i] = RealPink;
				}

				// And the first pixel to black to clear as it swipes upward
				maxBikeleds[0] = CRGB::Black;
			}

			// Scroll upwards
			for (int i= NUM_MAXBIKE_LEDS-1; i>0; i--)
			{
				maxBikeleds[i] = maxBikeleds[i-1];
			}
		}
		else if (mode == BIKESCROLL)
		{
			uint8_t palIndex = curPalOffset;
			for (int i=0; i<NUM_MAXBIKE_LEDS; i++)
			{
				maxBikeleds[i] = palmixer::finalPalette[palIndex];
				palIndex += 2; // two full palettes along the pole
			}

			curPalOffset--;
		}
		else if (mode == BIKESHIFT)
		{
			if (patternShiftTimer.GetEvent())
			{
				// Shift
				ShiftOut();

				// Introduce new pixels in middle
				SetMiddle(patternBuffer[patternCounter]);
				patternCounter++;
				if (patternCounter == patternSize) patternCounter = 0;
			}			
		}

		if (hueShiftTimer.GetEvent())
		{
			CRGB c1,c2,c3,c4;

			if (nearStructure)
			{
				hueA = random(150,180);
				hueB = random(210,250);

				satA = random(200,255);
				satB = random(200,255);

				valA = random(128,255);
				valB = random(128,255);

				CHSV c1 = CHSV(hueA, satA, valA);
				CHSV c2 = CHSV(hueB, satB, valB);
				SetStripesPattern(patternBuffer, patternSize, c1, CRGB::Black, c2, CRGB::Black);
			}
			else
			{
				// Try some sampling of the palette colors
				c1 = palmixer::finalPalette[curPalOffset];
				c2 = palmixer::finalPalette[curPalOffset + 12];
				c3 = palmixer::finalPalette[curPalOffset + 24];
				c4 = palmixer::finalPalette[curPalOffset + 36];
				SetStripesPattern(patternBuffer, patternSize, c1, c2, c3, c4);
			}
		}

	}

	void MaxBike::Render()
	{

	}

	// Some pattern definition utilities
	void MaxBike::SetStripesPattern(CRGB* buffer, int patternSize, CRGB c1, CRGB c2, CRGB c3, CRGB c4)
	{

		int stripeWidth = 8;
		for (int i=0; i<stripeWidth; i++)
		{
			buffer[i] = c1;
			buffer[i+stripeWidth] = c2;
			buffer[i+stripeWidth*2] = c3;
			buffer[i+stripeWidth*3] = c4;
		}
	}	
	void MaxBike::ShiftOut()
	{
		for (int i=0; i<MIDDLE1; i++)
			maxBikeleds[i] = maxBikeleds[i+1];
		for (int i=MIDDLE2; i>MIDDLE1+1; i++)
			maxBikeleds[i] = maxBikeleds[i-1];

		for (int i=MIDDLE1; i<MIDDLE2; i++)
			maxBikeleds[i] = maxBikeleds[i+1];
		for (int i=NUM_MAXBIKE_LEDS-1; i>MIDDLE2+1; i++)
			maxBikeleds[i] = maxBikeleds[i-1];

	}

	void MaxBike::SetMiddle(CRGB color)
	{
		maxBikeleds[MIDDLE1] = color;
		maxBikeleds[MIDDLE1+1] = color;

		maxBikeleds[MIDDLE2] = color;
		maxBikeleds[MIDDLE2+1] = color;
	}