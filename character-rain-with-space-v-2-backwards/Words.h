#ifndef WORDS_H
#define WORDS_H

#define MAX_CHARS 20

// A static class to hold "word" data for the character rain effect
typedef struct CharPos
{
	uint8_t strip; // 0 .. 7
	uint8_t character; // 0 .. 19
} CharPos;

class Words
{

	static CharPos characterPositions[];
	static uint8_t numCharacters;

	static void AddCharacter(uint8_t stripNum, uint8_t charNum);

public:

	// This routine sets up the character data
	static void Init(uint16_t controllerID);

	// Check to see if a particular character is in any of the special locations
	static bool IsInWord(uint8_t stripNum, uint8_t charNum);
};

#endif