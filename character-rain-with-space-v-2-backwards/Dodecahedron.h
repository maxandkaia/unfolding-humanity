#ifndef GEOMETRY
#define GEOMETRY

#include "MathUtil.h"
#include "constants.h"


// Class and methods for creating useful geometric access to the dodecahedron

#if 0
class Dodecahedron
{
public:
	// CONVENTIONS
	// 1) 'around the clock' is from the POV of above the dodec looking down
	// 2) the shape is centered at the R3 origin
	// 3) the radius of the shape (distance from origin to any vertex) is SQRT(3.0)
	// 
	// 
 //
 // The vertices of the dodecahedron, indexed in a specific order which goes as follows:
 // The five vertices around the top face, 0..4, and let's call vertex 0 12 oclock looking down at the shape from above
 // Then, all the 'middle' vertices, i.e. all not either on the top face or the bottom face, 5..14, going around the clock in 
 // a clockwise fashion, starting with the vertex that is most immediately clockwise from our 12 oclock top face vertex
 // then, the 'bottom face' vertices, 15..19, also going clockwise starting again with the vertex that is most immediately clockwise
 // from our 12 oclock top vertex when looking down from above
 // 
	static vec3 vertices[20];


	static int top[5]; // ordered around the clock from noon
	static int middle[10]; // ordered around the clock
	static int bottom[5]; // ordered around the clock

	//
	// Each vertex has 3 neighbors. Let's call these the clockwise, anticlockwise and up/down neighbors. Each neighbor can be classified as one
	// of these categories by virtue of whether or not it is in the same 'level' (top, middle, bottom) or in a different one. If it is in the same
	// level, then it is either a clockwise, or anticlockwise neighbor. We will use the convention that the clockwise neighbor comes first, followed
	// by the anticlockwise neighbor, followed by the up/down neighbor. It is the case that two of the neighbors will always be
	// clockwise/anticlockwise (these will be neighbors 0 and 1, respectively) and ONE will be a top/botom neighbor. All vertices in the top face
	// will have one bottom neighbor. All vertices in the bottom face will have one top neighbor, and all middle vertices will have one or the other,
	// alternating as you wind around the clock. Thus, if we drew a line from each vertex to it's two clockwise/anticlockwise neighbors, we would see
	// three outlines drawn on the shape. The perimeter of the top face, the perimeter of the bottom face, and a zig-zag line around the middle of
	// the shape. 
	//

	static int adjacency[3][20]; // 20 sets of adjacency information ordered as per comments above

	// We use the same scheme to catalog the edges. Each edge is a list of two vertices (by index) and we start at the top face, going clockwise
	// from our 12 oclock vertex, we have edges 0..4. Then, we have 'descending' edges from each of those vertices, starting with our 12 oclock
	// vertex. Then, we have the zig-zag middle belt of edges which connect the middle vertices, starting with the middle vertex most immediately
	// clockwise from our 12 oclock top vertex. Then we have descending edges that connext the middle belt with the bottom face, again starting with
	// the vertex most immediately clockwise from our 12 oclock top vertex. Then we have the bottom face perimeter edges, starting with the bottom
	// vertex that is most immediately clockwise from our 12 oclock vertex.
	static int edges[60];
	static vec3 segverts[5][NUM_LEDS_PER_EDGE_CHANNEL];

	// Method to compute and initialize all the above data structures
	static void Initialize(); 
	
};
#endif



#endif