#ifndef EDGECRAWL
#define EDGECRAWL

// The raindrops that we will be managing
#include <arduino.h>
#include "Effect.h"
#include "constants.h"
#include "tunables.h"
#include "timer.h"

// Raineffect owns some "lanes" which correspond to the strands. It spawns raindrops in random lanes. It knows which lanes have active raindrops and always chooses an empty lane in which to spawn
class EdgeCrawl : Effect
{
	// which segment I control
	int mySegment;

public:

	// This Init routine takes an argument of "which segment to control"
	void Init(int segment);

	void Update(unsigned long deltamics);

	void Render();
};


#endif