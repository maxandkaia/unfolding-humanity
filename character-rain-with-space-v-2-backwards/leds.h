#ifndef LEDS
#define LEDS
#include <arduino.h>
#include "constants.h"

#define USE_OCTOWS2811
#include <OctoWS2811.h>
#include "FastLED.h"


#ifdef DECLARE_GLOBALS
	CRGB *leds;
#else
	extern CRGB *leds;
#endif



inline void SetPixel(int strip, int led, CRGB color)
{
	leds[strip*LEDS_PER_PANEL_STRIP + led] = color;
}

inline void FadeAllStrips(uint8_t fadefactor)
{
	for (uint8_t stripnum=0; stripnum<NUM_PANEL_STRIPS; stripnum++)
	{
		int offset = stripnum * LEDS_PER_PANEL_STRIP;
		for (int i=0; i<LEDS_PER_PANEL_STRIP; i++)
			leds[offset + i].fadeToBlackBy(fadefactor);
	}
}


// line 0 on the Octo is the inside edges
// line 1 is the outside edges
// Octo divides the leds array into 8 equal arrays, but we only use the first 2, so we overallocate
// but its not a big deal, we just ignore the extras

#ifdef DECLARE_GLOBALS
	CRGB edgeleds[8][NUM_LEDS_PER_EDGE_CHANNEL];
#else
	extern 	CRGB edgeleds[8][NUM_LEDS_PER_EDGE_CHANNEL];
#endif

	// Put any edge related led utils here
#endif


