#ifndef STATUS_TRACKER
#define STATUS_TRACKER
#include <arduino.h>
#include "Controller.h"
#include "timer.h"
#include "CommsRouter.h"
#include "Constants.h"

// some structures to hold the comms status of all the child controllers. 
// We will update this and use it to detect if controllers have gone dark
struct Tracker
{
public:
	uint16_t controller_id;
	float secs_since_last_hb;
	const char* controller_name;

	void Init( const char* name, uint16_t id)
	{
		controller_id = id;
		controller_name = name;
		secs_since_last_hb = 0.0f;
	}
};

class StatusTracker
{

 Tracker statusTrackers[25];

	public:
		StatusTracker();

		void Update(unsigned long deltamics);

		void  UpdateStatus(uint16_t id);

		bool AllControllersOnline();

		// Print the status of all the controllers. Also, attempt to send it to the remote control
		void Report();

};

#endif
