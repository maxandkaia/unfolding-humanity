#include "leds.h"
#include "StarEffect.h"
#include "tunables.h"
#include "Fader.h"

CHSV StarEffect::starcolors[5];
CHSV baseflashbulbbg;
int cometthrottle; // how many cycles to wait until next move
int loopcount;
int comettail;
CRGB color1[2];
CRGB color2[2];

int comettailtarget = 15;


// An effect that makes a patternß "crawl" along the edges. In order to make it look nice even when the segments are not
// synchronized with one another, and to make it look nice even though the edges are addressed in a non-circuit way
// we will have the pattern emerge from every edge's center and move toward the vertices
void StarEffect::Init()
{
	glitterTimer.SetFrequency(TunablesManager::glitterfreq);
	glitterTimer.Start();
	starTimer.SetFrequency(TunablesManager::starfreq);
	starTimer.Start();
	cometTimer.SetFrequency(TunablesManager::cometfreq); 
	cometTimer.Start();
	cometthrottle = random8(2,4); 
	comettail = random8(3,30);
	comettailtarget = random8(3,30);
	Serial.print("     throttle, tail ");
	Serial.print(cometthrottle);
	Serial.println(comettail);
	loopcount = 0;

	starcolors[0] = CHSV(171, 120, 255);
	starcolors[1] = CHSV(192, 120, 255);
	starcolors[2] = CHSV(213, 120, 255);
	starcolors[3] = CHSV(234, 120, 255);
	starcolors[4] = CHSV(255, 120, 255);
	baseflashbulbbg = CHSV(140, 200, 80);

	color1[0] = CRGB(0, 25, 25);
	//color1[1] = CRGB(0, 25, 25);
	color2[0] = CRGB(25, 0, 25);
	//color2[1] = CRGB(0, 20, 10);

	for (int i = 0; i < NUMSTARS; i++) {
		stars[i].Init();
	}
}

// <<TODO>> Make star color tunable
// <<TODO>> Make star rate tunable
// <<TODO>> Make star fade tunable

int StarEffect::FindStar() {
	for (int i = 0; i < NUMSTARS; i++) {
		if (stars[i].state == DEAD) {
			return i;
		}
	}
	return -1;
}


void StarEffect::ResetAllStars() {
	for (int i = 0; i < NUMSTARS; i++) {
		stars[i].state = DEAD;
	}
}

void StarEffect::ClearAllEdges()
{
	for (int e=4; e<8; e++)
		for (int p=0; p<126; p++)
			edgeleds[e][p] = CRGB::Black;
}

// inside edge leds go from 0..NUM_LEDS_PER_EDGE_CHANNEL
void StarEffect::Update(unsigned long deltamics)
{
	uint8_t hue;
	uint8_t sat;

	//Serial.println("star effect update");
	glitterTimer.Update(deltamics);
	glitterTimer.SetFrequency(TunablesManager::glitterfreq);
	starTimer.Update(deltamics);
	starTimer.SetFrequency(TunablesManager::starfreq); // want to create a new tunable here
	cometTimer.Update(deltamics);
	cometTimer.SetFrequency(TunablesManager::cometfreq); // want to create a new tunable here

	//   GILTTER
	if ((TunablesManager::starmode == GLITTER) || (TunablesManager::starmode == COLORGLITTER)) {
		if (glitterTimer.GetEvent()) {
			for (uint8_t edge = 4; edge < 8; edge++) {
				for (uint8_t pixel = 0; pixel < 126; pixel++) { /// change to 210
					//if (edgeleds[edge][pixel].r > 128 && edgeleds[edge][pixel].g > 128 && edgeleds[edge][pixel].b > 128)
						edgeleds[edge][pixel] = CRGB::Black; //CRGB(5, 6, 5);
					if (random8(100) < TunablesManager::glitterdensity) {
						if (TunablesManager::starmode == COLORGLITTER) {
							hue = random8(130,175);
							sat = 100;
						}
						else {
							hue = 140;
							sat = 50;
						}
						edgeleds[edge][pixel] = CHSV(hue, sat, 255);
					}
				}
			}
		}
	}

	//   TWINKLE
	else if ((TunablesManager::starmode == TWINKLE) || (TunablesManager::starmode == COLORTWINKLE)) { // twinkle mode 
		for (int i = 0; i < NUMSTARS; i++) {
			stars[i].StarUpdate(deltamics);
		}

		if (starTimer.GetEvent()) {

			// spawn a new star
			int newstarindex = FindStar();
			if (newstarindex != -1) {
				uint8_t newedge = random8(4) + 4;
				uint8_t newpixel = random8(126); // change back to  NUM_LEDS_PER_EDGE_CHANNEL						
				stars[newstarindex].StarBirth(newedge, newpixel);
			}
		}
	}

	//   FLASHBULB
	else if ((TunablesManager::starmode == FLASHBULB) || (TunablesManager::starmode == RAINBOWFLASHBULB)) { 
		// set background color
		CHSV flashbg;


		for (uint8_t edge = 4; edge < 8; edge++) {

			if (TunablesManager::starmode == RAINBOWFLASHBULB) {
				uint8_t speed = beatsin8(6,0,255);
  				CRGB endclr = blend(color1[0], color2[0], speed);
  				CRGB midclr = blend(color2[0], color1[0], speed);
  				// first strip
				fill_gradient_RGB(edgeleds[edge], 0, endclr, 41/2, midclr);
				fill_gradient_RGB(edgeleds[edge], 41/2+1, midclr, 41, endclr);
				// second strip
				fill_gradient_RGB(edgeleds[edge], 42+0, endclr, 42+41/2, midclr);
				fill_gradient_RGB(edgeleds[edge], 42+41/2+1, midclr, 42+41, endclr);
				// third strip
				fill_gradient_RGB(edgeleds[edge], 84+0, endclr, 84+41/2, midclr);
				fill_gradient_RGB(edgeleds[edge], 84+41/2+1, midclr, 84+41, endclr);
			}
			else
			{
				flashbg = baseflashbulbbg;
				fill_solid(edgeleds[edge], 126, flashbg);
			}

			//for (uint8_t pixel = 0; pixel < 126; pixel++) {
			//	flashbg = baseflashbulbbg;
				// evolve the background color if rainbow
			//	if (TunablesManager::starmode == RAINBOWFLASHBULB) {
					//Serial.println ("rainbow flash mode!");
			//		flashbg.h = (baseflashbulbbg.h + loopcount/4) % 256;

			//	}
				//edgeleds[edge][pixel] = flashbg;
			//}
		}
		if (starTimer.GetEvent()) {
			// find somehwere random to flash

			uint8_t newedge = random8(4) + 4;
			uint8_t newpixel = random8(126); // change back to  NUM_LEDS_PER_EDGE_CHANNEL	

			edgeleds[newedge][newpixel] = CHSV(150, 200, 255);
			if (TunablesManager::starmode == RAINBOWFLASHBULB) {
				// evolve color

			}
			if (newpixel < 41) {                  // change to NUM_LEDS_PER_EDGE_CHANNEL - 1
				edgeleds[newedge][newpixel+1] = CHSV(150, 200, 255);
			}
		}
	}

	//   COMET 
	else if ((TunablesManager::starmode == COMET) || (TunablesManager::starmode == COLORCOMET)) {

		// Clear the edge segments to provide ablank slate
		//ClearAllEdges();

		// slow down comet motion
		if (loopcount % cometthrottle == 0) {
			// avoid overflow
			if (loopcount == 1000) {
				loopcount = 0;
			}

			//ClearAllEdges();

			for (int i = 0; i < NUMSTARS; i++) {
				stars[i].CometUpdate(deltamics, comettail);
			}

			if (cometTimer.GetEvent()) {
				// spawn a new comet

				int newstarindex = FindStar();
				//Serial.print("Launching star at index ");
				//Serial.println(newstarindex);
				if (newstarindex != -1) {

					// Semi random comet tail length
					if (comettail == comettailtarget)
					{
						comettailtarget = random(8,25);
					}
					else
					{
						// interpolate toward target
						if (comettailtarget > comettail)
							comettail++;
						else
							comettail--;
						Serial.print("comet tail ");
						Serial.println(comettail);
					}



					uint8_t newedge = random8(3) + 4;					
					stars[newstarindex].CometBirth(newedge, comettail);
				}
			}
		}
	}
	else if (TunablesManager::starmode == BURST) {
		uint8_t starthue = beatsin8(20, 0, 255);
  		uint8_t endhue = beatsin8(35, 0, 255);
  		for (uint8_t edge = 4; edge < 8; edge++) {
  			for (uint8_t seg=0; seg<4; seg++)
  			{
	  			if (starthue < endhue) {
	    			fill_gradient(&edgeleds[edge][seg*42], 42, CHSV(starthue,255,255), CHSV(endhue,255,255), FORWARD_HUES);  
	  			} else {
	   		 		fill_gradient(&edgeleds[edge][seg*42], 42, CHSV(starthue,255,255), CHSV(endhue,255,255), BACKWARD_HUES);
  				}
  			}
  		}
	}
	else if (TunablesManager::starmode == VERTEX) {
		// something cool
	}
	else {
		Serial.println("In StarEffect.cpp: bad star effect mode");
	}
	loopcount++;
}

void StarEffect::Render()
{

}
