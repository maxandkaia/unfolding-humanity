#ifndef LFO_H
#define LFO_H
#include <arduino.h>

 inline int MAX(int a,int b) {return ((a)>(b)?(a):(b));}
 inline int MIN(int a,int b) {return ((a)<(b)?(a):(b));}


class LFO
{

// Timer class just counts down to "event"
// When timer hits 0, event flag is set
// Event flag is only unset by fetching it, or by Reset
// Timer can either roll over and continue a new countdouwn, or just stop, depending on mode

enum LFOMode {
  SINE = 0,
  SAW = 1,
  PEAKS = 2
};

int bpm = 120; // 120 beats per minute by default
int min = 0;
int max = 255;
int range = 255;

LFOMode mode = SINE;

  public:

  LFO(LFOMode mode, int min, int max, int bpm);

  int Get(); // get the current value
};
#endif
