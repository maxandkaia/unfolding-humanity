#ifndef BURSTEFFECT
#define BURSTEFFECT

// The raindrops that we will be managing
#include <arduino.h>
#include "Effect.h"
#include "constants.h"
#include "tunables.h"
#include "timer.h"

// Raineffect owns some "lanes" which correspond to the strands. It spawns raindrops in random lanes. It knows which lanes have active raindrops and always chooses an empty lane in which to spawn
class BurstEffect : Effect
{

	// Start a burst every so often
	Timer burstTimer;

	// NOTE: the order of the strips is backward for left and right halves of a panel!
	int curLoc[NUM_PANEL_STRIPS];
	CRGB burstColor;

public:
	void Init();

	void Update(unsigned long deltamics);

	void Render();
};


#endif