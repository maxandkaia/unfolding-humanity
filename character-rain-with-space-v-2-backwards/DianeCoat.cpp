#define FASTLED_FORCE_SOFTWARE_SPI
#include "leds.h"
#include "DianeCoat.h"
#include "palmixer.h"

#include "power_mgt.h"

#define MIDDLE1 24
#define MIDDLE2 32

	// Some pattern definition utilities
	void DianeCoat::SetStripesPattern(CRGB* buffer, int patternSize, CRGB c1, CRGB c2, CRGB c3, CRGB c4)
	{
		Serial.println("stripes pattern");
		// Only works on patterns div by 4
		if (patternSize%8 != 0) return;

		int stripeWidth = 4;
		for (int i=0; i<stripeWidth; i++)
		{
			buffer[i] = c1;
			buffer[i+stripeWidth] = c2;
			buffer[i+stripeWidth*2] = c3;
			buffer[i+stripeWidth*3] = c4;


			buffer[i+stripeWidth*4] = c1;
			buffer[i+stripeWidth*5] = c2;
			buffer[i+stripeWidth*6] = c3;
			buffer[i+stripeWidth*7] = c4;

		}
	}

	void DianeCoat::Init()
	{
		FastLED.addLeds<OCTOWS2811>(dianeleds, NUM_DIANECOAT_LEDS);

		//FastLED.setBrightness(64);
		set_max_power_in_volts_and_milliamps(5,1000);

		patternShiftTimer.SetFrequency(20.0f);
		patternShiftTimer.Start();

		nearStructure = false;
		
		hueShiftTimer.SetFrequency(5.0f);
		hueShiftTimer.Start();

		modeChangeTimer.SetInterval(10.0f);
		modeChangeTimer.Start();

		SetStripesPattern(patternBuffer, patternSize, CRGB::Blue, CRGB::Black, CRGB::Red, CRGB::Black);
		
		mode = SCROLL;
		Serial.println("coat init; mode = scroll");
	}

	void DianeCoat::ShiftOut()
	{
		for (int i=0; i<MIDDLE1; i++)
			dianeleds[i] = dianeleds[i+1];
		//for (int i=MIDDLE2; i>MIDDLE1+1; i++)
		//	dianeleds[i] = dianeleds[i-1];

		//for (int i=MIDDLE1; i<MIDDLE2; i++)
		//	dianeleds[i] = dianeleds[i+1];
		for (int i=NUM_DIANECOAT_LEDS-1; i>MIDDLE1; i--)
			dianeleds[i] = dianeleds[i-1];

	}

	void DianeCoat::SetMiddle(CRGB color)
	{
		dianeleds[MIDDLE1] = color;
		dianeleds[MIDDLE1+1] = color;

		dianeleds[MIDDLE2] = color;
		dianeleds[MIDDLE2+1] = color;
	}


	void DianeCoat::Update(unsigned long deltamics)
	{


		patternShiftTimer.Update(deltamics);
		hueShiftTimer.Update(deltamics);
		modeChangeTimer.Update(deltamics);

		// Always keep the palette moving along
		curPalOffset++;

		// change modes sometimes
		if (modeChangeTimer.GetEvent())
		{
			if (!nearStructure)
			{
				mode++;
				if (mode == NUM_DIANECOAT_MODES)
					mode = 0;
			}	
		} 

		if (mode == SCROLL)
		{
			// TODO: add more effects
			uint8_t palIndex = curPalOffset;
			for (int i=0; i<NUM_DIANECOAT_LEDS; i++)
				dianeleds[i] = palmixer::finalPalette[palIndex++];
		}
		else if (mode == PATTERN)
		{
			if (patternShiftTimer.GetEvent())
			{
				// Shift
				ShiftOut();

				// Introduce new pixels in middle
				SetMiddle(patternBuffer[patternCounter]);
				patternCounter++;
				if (patternCounter == patternSize) patternCounter = 0;
			}
		}

		if (hueShiftTimer.GetEvent())
		{
			CRGB c1,c2,c3,c4;

			if (nearStructure)
			{
				hueA = random(150,180);
				hueB = random(210,250);

				satA = random(200,255);
				satB = random(200,255);

				valA = random(128,255);
				valB = random(128,255);

				CHSV c1 = CHSV(hueA, satA, valA);
				CHSV c2 = CHSV(hueB, satB, valB);
				SetStripesPattern(patternBuffer, patternSize, c1, CRGB::Black, c2, CRGB::Black);
			}
			else
			{
				// Try some sampling of the palette colors
				c1 = palmixer::finalPalette[curPalOffset];
				c2 = palmixer::finalPalette[curPalOffset + 12];
				c3 = palmixer::finalPalette[curPalOffset + 24];
				c4 = palmixer::finalPalette[curPalOffset + 36];
				SetStripesPattern(patternBuffer, patternSize, c1, c2, c3, c4);
			}
		}
	}

	void DianeCoat::Render()
	{

	}

	void DianeCoat::ArriveStructure()
	{
		nearStructure = true;
	}

	void DianeCoat::LeaveStructure()
	{
		nearStructure = false;
	}
