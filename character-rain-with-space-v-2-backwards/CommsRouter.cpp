//#define DEBUG
#include "debug_utils.h"
#include "constants.h"
#include "MessageHandler.h"
#include "CommsRouter.h"

void PrintHeader(RF24NetworkHeader* header)
{
	printf("Network Header:\n");
	printf("  from   : %d\n", header->from_node);
	printf("  to     : %d\n", header->to_node);
	printf("  msgid  : %d\n", header->id);
	printf("  type   : %d\n", header->type);
}

	CommsRouter::CommsRouter(uint16_t network_address, MessageHandler* _handler)
	{
		radio = new RF24(9,10);

		network = new RF24Network(*radio);
		network->multicastRelay = true;
	//	if (network_address == REMOTE_ID || network_address == DIANES_COAT || network_address == MAXS_COAT || network_address == MAXS_BIKE)
	//		network->multicastRelay = false;	
		this_address = network_address;



		handler = _handler;
	}

// Moved the controller pointer and net address init to here so we can construct both controller and router without a problem
	void CommsRouter::Initialize()
	{
		// Startup the radio and network
		SPI.begin();
		radio->begin();

		radio->setDataRate( RF24_250KBPS );
		radio->setPALevel(RF24_PA_MAX);

		//radio->printDetails();

		network->begin(/*channel*/ RADIO_CHANNEL, /*node address*/ this_address);

		radio->printDetails();

		Serial.print("Variant: "); Serial.println(radio->isPVariant());

	}
	
	void CommsRouter::Reset()
	{
		// Reset the radio and network
	}


	void CommsRouter::Update(unsigned long deltamics)
	{
		//DPRINT("Updating the network");
		// Read anything off the network and push into message queue of our controller
		network->update();
		//DPRINT("Done updating network");


		while ( network->available() )
		{     // Is there anything ready for us?
			RF24NetworkHeader header;        // If so, grab it and print it out
			char payload[256];
			char* payloadPtr = &payload[0];

			int bytesReceived = network->read(header,payloadPtr,256);
			
			//PrintHeader(&header);

			DPRINT("==================== Received message ================");
			DPRINT("Bytes count: ");
			DPRINT(bytesReceived);
			// Ensure NULL termination
			payload[bytesReceived] = 0x0;
			DPRINT("Payload is:");
			DPRINT(payloadPtr);
			DPRINT("Done reading payload from network");
			DPRINT("======================================================");

			if (handler)
			{
				handler->HandleMessage(payloadPtr, false);
			}

			// DPRINT("Pushing message onto queue");
			// if (strlen(payloadPtr))
			// 	queue->PushMessage(payloadPtr);
			// DPRINT("Done pushing message onto queue");
		}

	}

	// Send a message to a specific controller, use properly null terminated string as the payload!
	bool CommsRouter::SendMessageToController(uint16_t destination, const char* message)
	{
		if (strlen(message) == 0) return false;


// TEST Try using non-acked message format for performance
//	    RF24NetworkHeader header(/*to node*/ destination, 127);

		// RESULT: Skipping the ack by using mesage type 1 improves worst case performance by orders of magnitude. Since we are doing
		// our own comms status monitoring via heartbeating, we can skip the acks and go for the performance boost! (IMPORTANT since we need smooth 60FPS simulation and rendering!)
	    RF24NetworkHeader header(/*to node*/ destination, 127);
	    DPRINT("================ Writing message to network =================");
	    DPRINT("Message is: ");
	    DPRINT(message);
	    DPRINT("Numbytes will be: ");
	    DPRINT(strlen(message)+1);
	    bool result =  network->write(header,message,strlen(message)+1); // remember to transmit the NULL character as well. We only want properly terminated strings anywhere in the system!
	    DPRINT("Done writing message to network");
	    DPRINT("Result was: ");
	    DPRINT(result);
	    DPRINT("=============================================================");
	    return result;
	}

	// Send a message to a specific controller, use properly null terminated string as the payload!
	bool CommsRouter::SendMessageToControllerNoAck(uint16_t destination, const char* message)
	{
		if (strlen(message) == 0) return false;


// TEST Try using non-acked message format for performance
//	    RF24NetworkHeader header(/*to node*/ destination, 127);

		// RESULT: Skipping the ack by using mesage type 1 improves worst case performance by orders of magnitude. Since we are doing
		// our own comms status monitoring via heartbeating, we can skip the acks and go for the performance boost! (IMPORTANT since we need smooth 60FPS simulation and rendering!)
	    RF24NetworkHeader header(/*to node*/ destination, 1);
	    DPRINT("================ Writing message to network =================");
	    DPRINT("Message is: ");
	    DPRINT(message);
	    DPRINT("Numbytes will be: ");
	    DPRINT(strlen(message)+1);
	    bool result =  network->write(header,message,strlen(message)+1); // remember to transmit the NULL character as well. We only want properly terminated strings anywhere in the system!
	    DPRINT("Done writing message to network");
	    DPRINT("Result was: ");
	    DPRINT(result);
	    DPRINT("=============================================================");
	    return result;
	}

	bool CommsRouter::SendMulticast(const char* message)
	{
	    RF24NetworkHeader header(/*multicast addr should not matter*/ 0, 127);
		bool result = network->multicast(header,message,strlen(message)+1, 1); // this multicast should get relayed to every controller!
	    DPRINT("Done writing multicast to network");
	    DPRINT("Result was: ");
	    DPRINT(result);
	    DPRINT("=============================================================");
	    return result;		
	}

	bool CommsRouter::MulticastToAllPanels(const char* message)
	{
		SendMulticast(message);
		return true;
	}


	bool CommsRouter::SendMessageToAllPanels(const char* message)
	{

		SendMessageToController(TOP_EDGE_1, message);
		SendMessageToController(TOP_EDGE_2, message);
		SendMessageToController(TOP_EDGE_3, message);
		SendMessageToController(TOP_EDGE_4, message);
		SendMessageToController(TOP_EDGE_5, message);

		SendMessageToController(TOP_PANEL_1_A, message);
		SendMessageToController(TOP_PANEL_1_B, message);

		SendMessageToController(TOP_PANEL_2_A, message);
		SendMessageToController(TOP_PANEL_2_B, message);

		SendMessageToController(TOP_PANEL_3_A, message);
		SendMessageToController(TOP_PANEL_3_B, message);

		SendMessageToController(TOP_PANEL_4_A, message);
		SendMessageToController(TOP_PANEL_4_B, message);

		SendMessageToController(TOP_PANEL_5_A, message);
		SendMessageToController(TOP_PANEL_5_B, message);

		SendMessageToController(BOTTOM_PANEL_1_A, message);
		SendMessageToController(BOTTOM_PANEL_1_B, message);

		SendMessageToController(BOTTOM_PANEL_2_A, message);
		SendMessageToController(BOTTOM_PANEL_2_B, message);

		SendMessageToController(BOTTOM_PANEL_3_A, message);
		SendMessageToController(BOTTOM_PANEL_3_B, message);

		SendMessageToController(BOTTOM_PANEL_4_A, message);
		SendMessageToController(BOTTOM_PANEL_4_B, message);

		SendMessageToController(BOTTOM_PANEL_5_A, message);
		SendMessageToController(BOTTOM_PANEL_5_B, message);

		SendMessageToController(REMOTE_ID, message);
		SendMessageToController(MAXS_COAT, message);
		SendMessageToController(DIANES_COAT, message);


		// <<TODO>> Add error return to all the above calls
		return true;
	}

	bool CommsRouter::SendMessageToMaster(const char* message)
	{
		return SendMessageToController(MASTER_ID, message);
	}

	bool CommsRouter::SendMessageToAllTopEdges(const char* message)
	{
		SendMessageToController(TOP_EDGE_1, message);
		SendMessageToController(TOP_EDGE_2, message);
		SendMessageToController(TOP_EDGE_3, message);
		SendMessageToController(TOP_EDGE_4, message);
		SendMessageToController(TOP_EDGE_5, message);

		// <<TODO>> Add error return to all the above calls!
		return true;
	}

