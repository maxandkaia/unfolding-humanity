
#include "palmixer.h"
#include "palette_data.h"

// In this class we animate some faders or increments
// and then support mixing of the two renderbuffers into the frontbuffer
// Which then gets replicated (or not) and displayed

  float palmixer::fader = 0.0f; // Goes from 0.0f to 1.0f
  float palmixer::deltaFade; // amount to fade per second
  bool palmixer::active = false; // are we currently animating anything?
  fract8 palmixer::fraction;
  
  // An array of palette pointers so we can randomly choose one
  //#define kNumPalettes 64
  CRGBPalette16 palmixer::palettes[kNumPalettes];

  // when we want to blend to a new palette, we assign to nextPalette, then use nblend to modify the target palette over time.
  // all color fetch functions use the final palette in some way or another
  CRGBPalette256 palmixer::nextPalette;
  CRGBPalette256 palmixer::curPalette;
  CRGBPalette256 palmixer::finalPalette;

  void palmixer::GenerateGlobalPalettes()
  {
    palettes[kClouds] = CloudColors_p;
    palettes[kRainbow] = RainbowColors_p;
    palettes[kParty] = PartyColors_p;
    palettes[kPurpleFly] = purplefly_gp;
    palettes[kIris] = iris_gp;
    palettes[kMadras] = madras_gp;
    palettes[kLailah] = lailah1_gp;
    palettes[kHope] = hopegoddess_gp;
    palettes[kCompassion] = angelcompassion_gp;
    palettes[kRepose] = angelrepose_gp;
    palettes[kMermaid] = aquamarinemermaid_gp;
    palettes[kFlame] = flame_gp;
    palettes[kSerendil] = serendil_gp;
    palettes[kDaisyFae] = daisyfae_gp;
    palettes[kFaeCat] = faecat_gp;
    palettes[kFireIce] = fireandice_gp;
    palettes[kHangOn] = hangonfatboy_gp;
    palettes[kMistress] = mistressnight_gp;
    palettes[kOtis] = otis_gp;
    palettes[kScoutie] = scoutie_gp;
    palettes[kSunlitWave] = sunlitwave_gp;
    palettes[kTrove] = trove_gp;
    palettes[kBlackhorse] = blackhorse_gp;
    palettes[kPlumbago] = plumbago_gp;
    palettes[kSchwarzwald] = schwarzwald_gp;
    palettes[kNrwc] = nrwc_gp;
    palettes[kEpochs] = epochs_gp;
    palettes[kSky04] = sky_04_gp;
    palettes[kSky12] = sky_12_gp;
    palettes[kSky34] = sky_34_gp;

  // Make sure all palettes are initialized
    finalPalette = palettes[kSchwarzwald];
    curPalette = finalPalette;
    nextPalette = curPalette;
  }
  
  void palmixer::Update(float dt)
  {
  	if (active)
  	{
  		// For now just one animate mode, crossfade
  		float fadeIncrement = deltaFade * dt;
  		fader += fadeIncrement;
  		fraction = (uint8_t)(fader * 255.0f);
  		if (fader > 1.0f)
  		{
  			fader = 1.0f;
  			fraction = 255;
  			active = false;
  		}

  //  		Serial.printf("Blending: %d",fraction);
    		// Use blend to move toward target palette
    		for (int i=0; i<256; i++)
    		{
    			finalPalette[i] = blend(curPalette[i], nextPalette[i], fraction);
    		}
  	}
  }
  
  void palmixer::SetNewPalette(uint8_t newPal, float seconds)
  {
  	
  	// guard
  	if (newPal >= kNumPalettes) return;

  	curPalette = finalPalette;
  	nextPalette = palettes[newPal];

  	fader = 0.0f;
  	fraction = 0;
  	deltaFade = 1.0f/seconds;
    active = true;
  }
  

