#ifndef MASTER_CONTROLLER
#define MASTER_CONTROLLER
#include <arduino.h>
#include "Controller.h"
#include "timer.h"
#include "CommsRouter.h"
#include "StatusTracker.h"


class MasterController : Controller
{

  // This is the timer for sending MHB (Master Heart Beat) out to all the children controllers. They will go to default mode if they don't hear from the master
  // on a regular cadence
  Timer heartbeatTimer;

  Timer dianeTimer;
  Timer maxTimer;

  Timer burstTriggerTimer;
  Timer burstDurationTimer;
  bool inBurstMode;

  Timer edgeModeChangeTimer;

  Timer glitterSpikeTimer; // every so often we will trigger a glitter spike

  bool dianeIsHere;
  bool maxIsHere;

  StatusTracker statusTracker;

	public:
		MasterController(uint16_t id, const char* name);

		// Update will process any inputs (sensors, etc), perform comms on the radios, process any commands in the command queue
		void Update(unsigned long deltamics);

		// Do any post-construction setup (network initialize etc)
		void Initialize();

		void HandleMessage(const char* message, bool fromSerial);

		// Render will take the current state of simulation and render it to the led strips (if any)
		void Render();
		// Reset will take us back to the state just after power on (typically only used if something goes wrong)
		void Reset();
};

#endif
