#ifndef TOP_EDGE_CONTROLLER
#define TOP_EDGE_CONTROLLER
#include <arduino.h>
#include "leds.h"
#include "Controller.h"
#include "EdgeTest.h"
#include "EdgeCrawl.h"
#include "EdgeEffect.h"
#include "StarEffect.h"

#include "Pulse.h"



class TopEdgeController : Controller
{
	EdgeTest edgeTest;
	EdgeEffect edgeEffect;
	EdgeCrawl edgeCrawl;
	StarEffect starEffect;

	Timer maxmodeTimer;
	Timer burstmodeTimer;

	Pulse glitterPulse;

	int mySegment; // which segment of leds I control

	public:
		TopEdgeController(uint16_t id, const char* name);

		// Update will process any inputs (sensors, etc), perform comms on the radios, process any commands in the command queue
		void Update(unsigned long deltamics);

		virtual void HandleMessage(const char* message, bool fromSerial);

		// Do any post-construction setup (network initialize etc)
		void Initialize();

		// Render will take the current state of simulation and render it to the led strips (if any)
		void Render();

		// Reset will take us back to the state just after power on (typically only used if something goes wrong)
		void Reset();
};

#endif