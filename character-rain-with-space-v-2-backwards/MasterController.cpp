//#define DEBUG
#include "debug_utils.h"
#include "print.h"
#include "constants.h"
#include "MasterController.h"
#include "timer.h"
#include "CommsRouter.h"


// Hacky, but needed for the edge star modes for now
#include "star.h"
#define NUMMODES 8
static float glitterDuration = 20.0f;
static float twinkleDuration = 20.0f;
static float flahsbulbDuration = 30.0f;
static float cometDuration = 40.0f;
static float colorcometDuration = 40.0f;
static float colorglitterDuration = 10.0f;
static float colortwinkleDuration = 20.0f;
static float rainbowflashbulbDuration = 30.0f;

		MasterController::MasterController(uint16_t id, const char* name) : Controller(id, name)
		{

      		heartbeatTimer.SetInterval(2.0f);
      		heartbeatTimer.Start();

      		// do a burst every minute if either Diane or Max is around
      		burstTriggerTimer.SetInterval(59.0f);
      		burstTriggerTimer.Start();

      		burstDurationTimer.SetInterval(10.0f);

      		glitterSpikeTimer.SetInterval(10.0f);
      		glitterSpikeTimer.Start();

      		dianeTimer.SetInterval(30.0f);
      		dianeTimer.Start();

      		maxTimer.SetInterval(31.0f);
      		maxTimer.Start();

      		edgeModeChangeTimer.SetInterval(30.0f);
      		edgeModeChangeTimer.Start();

      		dianeIsHere = false;
      		maxIsHere = false;

      		inBurstMode = false;
		}

		// Do any post-construction setup (network initialize etc)
		void MasterController::Initialize()
		{
			Controller::Initialize();	
		}

		// Update will process any inputs (sensors, etc), perform comms on the radios, process any commands in the command queue
		void MasterController::Update(unsigned long deltamics)
		{
			//Serial.println("Master Update");
			// Update the base class (the comms router gets updated this way)
			Controller::Update(deltamics);

			heartbeatTimer.Update(deltamics);

			statusTracker.Update(deltamics);

			dianeTimer.Update(deltamics);
			maxTimer.Update(deltamics);

			glitterSpikeTimer.Update(deltamics);

			burstTriggerTimer.Update(deltamics);
			burstDurationTimer.Update(deltamics);

			edgeModeChangeTimer.Update(deltamics);


			// Send a heartbeat signal every second. If edges/panels fail to get this, they will switch to default mode until they get reconnected
			if (heartbeatTimer.GetEvent())
			{
				//Serial.println("Hearbeat...");
				router->MulticastToAllPanels("MHB");
			}

			// Every once in a while ask the edges to "spike" the glitter density
			if (glitterSpikeTimer.GetEvent())
			{
				router->MulticastToAllPanels("glitterspike");
			}

			if (dianeTimer.GetEvent())
			{
				if (dianeIsHere)
				{
					Serial.println("Diane is no longer here...");
					dianeIsHere = false;
					MessageRetry("remove_diane",3);
				}

				// As a fallback just send the remove message every interval. That way, even if there 
				// are comms issues, the structure will eventually revert to it's default state
				router->SendMessageToAllPanels("remove_diane");
			}

			if (maxTimer.GetEvent())
			{
				if (maxIsHere)
				{
					Serial.println("Max is no longer here...");
					maxIsHere = false;
					MessageRetry("remove_max",3);
				}

				// As a fallback just send the remove message every interval. That way, even if there 
				// are comms issues, the structure will eventually revert to it's default state
				router->SendMessageToAllPanels("remove_max");
			}

			if (burstTriggerTimer.GetEvent() && (dianeIsHere || maxIsHere))
			{
				MessageRetry("burstmode",3);
				burstDurationTimer.Start();
				inBurstMode = true;
			}

			if (burstDurationTimer.GetEvent())
			{
				inBurstMode = false;
				burstDurationTimer.Stop();
				burstDurationTimer.Reset();
			}

			if (edgeModeChangeTimer.GetEvent() && !inBurstMode)
			{
				// TODO Send a different message if diane or max are here!!!
				//TunablesManager::starmode = random8(NUMMODES);
				// TO DO: comment this next line out for randomness
				TunablesManager::starmode = (TunablesManager::starmode + 1) % NUMMODES;
				switch(TunablesManager::starmode) {
					case GLITTER:
						Serial.println("Glitter star mode");
						EdgeMessageRetry("glitter", 5);
						edgeModeChangeTimer.SetInterval(glitterDuration);
						break;
					case TWINKLE:
						Serial.println("Twinkle star mode");
						EdgeMessageRetry("twinkle", 5);
						edgeModeChangeTimer.SetInterval(twinkleDuration);
						break;
					case FLASHBULB:
						Serial.println("Flashbulb star mode");
						EdgeMessageRetry("flashbulb", 5);
						edgeModeChangeTimer.SetInterval(flahsbulbDuration);
						break;
					case COMET:
						Serial.println("Comet star mode");
						EdgeMessageRetry("comet", 5);
						edgeModeChangeTimer.SetInterval(cometDuration);
						break;
					case COLORGLITTER:
						Serial.println("Glitter star mode");
						EdgeMessageRetry("colorglitter", 5);
						edgeModeChangeTimer.SetInterval(glitterDuration);
						break;
					case COLORTWINKLE:
						Serial.println("Twinkle star mode");
						EdgeMessageRetry("colortwinkle", 5);
						edgeModeChangeTimer.SetInterval(twinkleDuration);
						break;
					case RAINBOWFLASHBULB:
						Serial.println("Flashbulb star mode");
						EdgeMessageRetry("rainbowflashbulb", 5);
						edgeModeChangeTimer.SetInterval(flahsbulbDuration);
						break;
					case COLORCOMET:
						Serial.println("Color Comet star mode");
						EdgeMessageRetry("colorcomet", 5);
						edgeModeChangeTimer.SetInterval(colorcometDuration);
						break;
					default:
						Serial.println("Invalid star mode chosen");
						break;
				}
			}
		}

		void MasterController::HandleMessage(const char* message, bool fromSerial)
		{
			//Serial.println("Handling message:");
			//Serial.println(message);
			if (!strncmp(message,"HB",2))
			{
				// Somebody is reporting in, account for them
				unsigned int id;
				int ret = sscanf(message,"HB %u", &id);
				if (ret == 1)
				{
					//Serial.println(message);
					statusTracker.UpdateStatus((uint16_t)id);
				}
			}
			else if (!strcmp(message,"report"))
			{
				statusTracker.Report();
			}
			else if (!strcmp(message,"dianeishere"))
			{
				if (!dianeIsHere)
				{
					Serial.println("Diane is here...");
					dianeIsHere = true;
					//router->SendMessageToAllPanels("add_diane");
					MessageRetry("add_diane",3);
				}

				// Send the add diane message repeatedly as the panels will revert to default in the 
				// absence of this event (60 seconds timeout)
				router->SendMessageToAllPanels("add_diane");

				dianeTimer.Reset(); // This resets the timer and prevents an event for the next 30 seconds

			}
			else if (!strcmp(message,"maxishere"))
			{
				Serial.println("maxishere event");
				if (!maxIsHere)	
				{
					Serial.println("Max is here...");
					//router->SendMessageToAllPanels("add_max");
					MessageRetry("add_max", 3);
					maxIsHere = true;
				}

				// Send the add diane message repeatedly as the panels will revert to default in the 
				// absence of this event (60 seconds timeout)
				router->SendMessageToAllPanels("add_max");

				maxTimer.Reset(); // This resets the timer and prevents an event for the next 30 seconds

				// Forward the message 
			}
			else
			{
				// For now, just send it to the base class. Later handle any master specific messages
				Controller::HandleMessage(message, fromSerial);
			}
		}

		// Render will take the current state of simulation and render it to the led strips (if any)
		void MasterController::Render()
		{
			// TODO
		}

		// Reset will take us back to the state just after power on (typically only used if something goes wrong)
		void MasterController::Reset()
		{
			Controller::Reset();
		}

