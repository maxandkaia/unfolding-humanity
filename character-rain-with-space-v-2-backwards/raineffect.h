#ifndef RAINEFFECT
#define RAINEFFECT

// The raindrops that we will be managing
#include <arduino.h>
#include "Effect.h"
#include "constants.h"
#include "tunables.h"
#include "raindrop.h"
#include "timer.h"

// Raineffect owns some "lanes" which correspond to the strands. It spawns raindrops in random lanes. It knows which lanes have active raindrops and always chooses an empty lane in which to spawn
class RainEffect : Effect
{
public:

	// number of drops in this lane
	uint8_t laneOccupancy[NUM_PANEL_STRIPS];

	raindrop drops[MAX_DROPS];
	Timer spawnTimer;
	bool topPanel;

	uint8_t fadeFactors[NUM_PANEL_STRIPS][20];

	bool rainbowMode;

	// Returns -1 if no lane available
	int FindOpenLane();

	void UpdateLaneActivity();

	int FindFreeDrop();

	void RandomizeFadeFactors(uint8_t laneNumber);

	void FadeAllCharacters();


public:
	void Init();

	void Update(unsigned long deltamics);

	void Render();
};


#endif