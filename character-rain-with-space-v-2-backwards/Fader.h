#ifndef FADER_H
#define FADER_H
#include <arduino.h>
class Fader
{

// Fader class will ramp a value from A to B over time


  public:

  float startValue;
  float endValue;
  float increment; // How much to change per second
  float curValue;
  bool finished;


  void Init(float A, float B, float seconds);

  void Update(unsigned long mics);
  
  float GetValue();

  bool IsFinished();

  
};
#endif
