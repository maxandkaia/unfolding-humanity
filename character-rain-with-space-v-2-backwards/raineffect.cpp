
// The raindrops that we will be managing
#include "leds.h"
#include "raineffect.h"
#include "number_utils.h"

#include "Words.h"

bool fadeFlag = true;

	void RainEffect::FadeAllCharacters()
	{
		for (int stripnum=0; stripnum<NUM_PANEL_STRIPS; stripnum++)
		{
			for (int charposition=0; charposition<20; charposition++)
			{
				uint8_t start = (charposition*6); // Get the first led of the character
				for (int i=0; i<6; i++)
				{
					leds[stripnum*LEDS_PER_PANEL_STRIP + start + i].fadeToBlackBy(fadeFactors[stripnum][charposition]);
				}
			}
		}
	}

	void RainEffect::RandomizeFadeFactors(uint8_t lane)
	{
		uint8_t lanefade = random(3,10);
		for (int i=0; i<20; i++)

			fadeFactors[lane][i] = random(TunablesManager::rainfademin, TunablesManager::rainfademax) + lanefade;
			//fadeFactors[lane][i] = random(1, 20);
	}



	// Returns -1 if no lane available
	int RainEffect::FindOpenLane()
	{
		// pick random lane to start with and then wrap around until we have checked all lanes
		// NOTE: each controller only drives a subset of the lanes. Use that info to optimize the checks etc.
		uint8_t curLane = random(0,NUM_PANEL_STRIPS);

		for (int i=0; i<NUM_PANEL_STRIPS; i++)
		{
			if (laneOccupancy[curLane] < 1)
				return curLane;

			curLane++;
			if (curLane == NUM_PANEL_STRIPS)
				curLane = 0;
		}

		// Didn't find an empty lane
		return -1;
	}

	void RainEffect::UpdateLaneActivity()
	{
		// Update the lane empty flags to empty first
		for (int i=0; i<NUM_PANEL_STRIPS; i++)
		{
			laneOccupancy[i] = 0;
		}

		// Check all drops, and for active ones, mark the lane they are in as busy
		for (int i=0; i<MAX_DROPS; i++)
		{
			if (drops[i].active)
				laneOccupancy[drops[i].stripnum] += 1;
		}		
	}

	int RainEffect::FindFreeDrop()
	{
		for (int i=0; i<MAX_DROPS; i++)
		{
			if (!drops[i].active)
				return i;
		}
		return -1;
	}


	void RainEffect::Init()
	{

		// Do setup work
		for (int i=0; i<NUM_PANEL_STRIPS; i++)
		{
			laneOccupancy[i] = 0;
			RandomizeFadeFactors(i);
		}
		for (int i=0; i<MAX_DROPS; i++)
		{
			drops[i].active = false;
		}
		unsigned long randomTime = random(TunablesManager::minspawnwait,TunablesManager::maxspawnwait+1);
		spawnTimer.SetInterval(randomTime);
		spawnTimer.Start();

		rainbowMode = false;

	}


	void RainEffect::Update(unsigned long deltamics)
	{
		static uint8_t rainhue = 0;

		// Update all the drops
		for (int i=0; i<MAX_DROPS; i++)
		{
			if (drops[i].active)
				drops[i].Update(deltamics);
		}

		// Update the spawn timer and when it is elapsed, try to spawn a drop
		spawnTimer.Update(deltamics);
		if (spawnTimer.GetEvent())
		{
			// Reset with random wait interval
			unsigned long randomTime = random(TunablesManager::minspawnwait,TunablesManager::maxspawnwait+1);
			spawnTimer.SetInterval(randomTime);

			// Update lane occupancy
			UpdateLaneActivity();

			// Find an empty lane (if any)
			int emptyLane = FindOpenLane();
			if (emptyLane == -1) return; // Bail if no open lane

			// Spawn a drop there (if any free drops)
			int freedrop = FindFreeDrop();
			if (freedrop >= 0)
			{
				// TUNE these for better effect!!
				uint8_t startChar = 18;
				if (topPanel) startChar = 0;

				//Serial.print("dropping at charpos: "); Serial.println(startChar);
				float speed = utils::frand(TunablesManager::mindropspeed, TunablesManager::maxdropspeed);

				RandomizeFadeFactors(emptyLane);

				 CRGB dropcolor = TunablesManager::dropcolor;
				 CRGB trailcolor = TunablesManager::trailcolor;
				 if (rainbowMode)
				 {

				 	uint8_t hue = random(rainhue-10,rainhue+10);
				 	CHSV c =  CHSV(hue, 128, 255);
				 	dropcolor = c;
				 	c.s = 255;
				 	c.v = 128;
				 	trailcolor = c;
				 	rainhue += 5;
				 }

				drops[freedrop].Launch(emptyLane, startChar, speed, dropcolor, trailcolor, topPanel); // Launch with some params
			}

		}

		// Tune this or better yet run at fixed update rate and then tune it
		if (fadeFlag)
		{
			FadeAllCharacters();
		}
		fadeFlag = !fadeFlag;
	}

	void RainEffect::Render()
	{
		// For now the rendering is actually done in the Update call. May want to refactor that to fit the Effect class pattern better, but not a huge deal...
	}