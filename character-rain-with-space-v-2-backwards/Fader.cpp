
#include "Fader.h"

  void  Fader::Init(float A, float B, float seconds)
  {
  	startValue = A;
  	endValue = B;
  	increment = (endValue - startValue)/seconds;
  	curValue = A;
  	finished = false;
  }

  void  Fader::Update(unsigned long mics)
  {
  	// Bail if already done
  	if (finished) return;

  	// Get float version of deltaT as seconds
  	float deltaSeconds = (float)(mics) / 1000000.0f;
  	curValue += deltaSeconds * increment;

  	// Check to see if we are done
  	if (increment > 0.0f)
  	{
  		if (curValue >= endValue)
  		{
  			curValue = endValue;
  			finished = true;
  		}
  	}
  	else
  	{
  		if (curValue <= endValue)
  		{
  			curValue = endValue;
  			finished = true;
  		}
  	}
  }

  float  Fader::GetValue()
  {
  	return curValue;
  }

  bool  Fader::IsFinished()
  {
  	return finished;
  }

