#define DEBUG
#include "debug_utils.h"
#include "print.h"
#include "constants.h"
#include "RemoteController.h"
#include "timer.h"
#include "CommsRouter.h"

		RemoteController::RemoteController(uint16_t id, const char* name) : Controller(id, name)
		{
			nearStructure = false;
		}

		// Do any post-construction setup (network initialize etc)
		void RemoteController::Initialize()
		{
			coatTimer.SetInterval(5.0f);
			if (my_address == DIANES_COAT || my_address == MAXS_COAT || my_address == MAXS_BIKE)
			{
				coatTimer.Start();
			}
			Controller::Initialize();

			if (my_address == MAXS_COAT)
			{
				maxCoatEffect.Init();				
			}
			else if (my_address == MAXS_BIKE)
			{
				maxBikeEffect.Init();
			}
			else if (my_address == DIANES_COAT)
			{
				dianeCoatEffect.Init();
			}
		}

		// Update will process any inputs (sensors, etc), perform comms on the radios, process any commands in the command queue
		void RemoteController::Update(unsigned long deltamics)
		{

			if (my_address == MAXS_COAT)
			{
				maxCoatEffect.Update(deltamics);
			} 
			else if (my_address == MAXS_BIKE)
			{
				maxBikeEffect.Update(deltamics);
			}
			else if (my_address == DIANES_COAT)
			{
				dianeCoatEffect.Update(deltamics);
			}

			// Update the base class (the comms router gets updated this way)
			Controller::Update(deltamics);

			
			coatTimer.Update(deltamics);
			if (coatTimer.GetEvent())
			{
				Serial.println("Sending presence message to Master...");
				if (my_address == DIANES_COAT)
					router->SendMessageToMaster("dianeishere");
				else if (my_address == MAXS_COAT)
					router->SendMessageToMaster("maxishere");
				else if (my_address == MAXS_BIKE)
					router->SendMessageToMaster("maxbikeishere");
			}

			if (Controller::commsBlackoutMode && nearStructure)
			{
				Serial.println("Leaving structure...");
				nearStructure = false;
				maxCoatEffect.LeaveStructure();
			}
			else if (!Controller::commsBlackoutMode && !nearStructure)
			{
				Serial.println("Nearing structure...");
				nearStructure = true;
				maxCoatEffect.ArriveStructure();
			}


		}

		void RemoteController::HandleMessage(const char* message, bool fromSerial)
		{
			if (fromSerial)
			{
				if (!strcmp(message,"announce"))
				{
					Serial.println(my_name);
				}
				else
				{
					Serial.print("You entered: "); Serial.println(message);
					Serial.println("Passing it on to the master controller...");
					router->SendMessageToMaster(message);
				}
			}
			else
			{
				Controller::HandleMessage(message, false);
			}
	}

		// Render will take the current state of simulation and render it to the led strips (if any)
		void RemoteController::Render()
		{
			// TODO
			FastLED.show() ;
		}

		// Reset will take us back to the state just after power on (typically only used if something goes wrong)
		void RemoteController::Reset()
		{
			Controller::Reset();
		}

