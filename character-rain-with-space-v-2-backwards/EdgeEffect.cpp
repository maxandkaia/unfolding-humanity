
#include "leds.h"
#include "EdgeEffect.h"


	// Some pattern definition utilities
	void SetStripesPattern(CRGB* buffer, int patternSize, CRGB c1, CRGB c2, CRGB c3, CRGB c4)
	{
		// Only works on patterns div by 4
		if (patternSize%4 != 0) return;

		int stripeWidth = patternSize/4;
		for (int i=0; i<stripeWidth; i++)
		{
			buffer[i] = c1;
			buffer[i+stripeWidth] = c2;
			buffer[i+stripeWidth*2] = c3;
			buffer[i+stripeWidth*3] = c4;
		}
	}

	void EdgeEffect::SetRainbowPattern()
	{
		for (int i=0; i<32; i++)
		{
			patternBuffer[i] = CHSV(i*8, 255, 255);
		}
	}

	void EdgeEffect::Init()
	{
		// We shift pixels along the edge at 20 per second
		edgeShiftTimer.SetFrequency(20.0f);
		edgeShiftTimer.Start();

		// We modify the hues and saturations etc every 5 seconds to add a little variation
		hueShiftTimer.SetFrequency(5.0f);
		hueShiftTimer.Start();

		mode = NORMALMODE;
	}

	void EdgeEffect::ShiftOut(int edgeNum, int lineNum)
	{
		int firstLed = NUM_LEDS_PER_EDGE*edgeNum;
		int lastLed = firstLed + 41;
		for (int i=0; i<20; i++)
		{
			edgeleds[lineNum][firstLed] = edgeleds[lineNum][firstLed + 1];
			edgeleds[lineNum][lastLed] = edgeleds[lineNum][lastLed - 1];
			firstLed++;
			lastLed--;
		}
	}

	void EdgeEffect::SetMiddle(int edgeNum, int lineNum, CRGB color)
	{
		int midLed = NUM_LEDS_PER_EDGE*edgeNum + 20;
		edgeleds[lineNum][midLed] = color;
		edgeleds[lineNum][midLed + 1] = color;

	}


	void EdgeEffect::Update(unsigned long deltamics)
	{
		edgeShiftTimer.Update(deltamics);
		hueShiftTimer.Update(deltamics);

		// Always shift and introduce new pixels
		if (edgeShiftTimer.GetEvent())
		{
			for (int line = 0; line < 4; line++)
			{
				// Shift all pixels from the center of the edge toward the vertices. Then, introduce a new pixel in the middle
				for (int i=0; i<NUM_EDGES_PER_SEGMENT; i++)
				{
					ShiftOut(i, line);
					SetMiddle(i, line, patternBuffer[patternCounter]);
				
				}
			}
			patternCounter++;
			if (patternCounter == patternSize) patternCounter = 0;
		}

		// Change colors in the pattern based on the mode
		if (hueShiftTimer.GetEvent())
		{

			if (mode == MAXMODE)
			{
				hueA = random(150,180);
				hueB = random(210,250);

				satA = random(200,255);
				satB = random(200,255);

				valA = random(128,255);
				valB = random(128,255);

				CHSV c1 = CHSV(hueA, satA, valA);
				CHSV c2 = CHSV(hueB, satB, valB);

				SetStripesPattern(patternBuffer, patternSize, c1, CRGB::Black, c2, CRGB::Black);
			}
			else if (mode == NORMALMODE)
			{
				hueA = random(85,171);
				hueB = random(85,171);

				satA = random(200,255);
				satB = random(200,255);

				valA = random(128,255);
				valB = random(128,255);

				CHSV c1 = CHSV(hueA, satA, valA);
				CHSV c2 = CHSV(hueB, satB, valB);

				SetStripesPattern(patternBuffer, patternSize, c1, CRGB::Black, c2, CRGB::Black);
			}
		}
	}

	void EdgeEffect::Render()
	{

	}
