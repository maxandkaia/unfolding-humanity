#ifndef TUNABLES
#define TUNABLES
#include <arduino.h>
#include "FastLED.h"



 class TunablesManager
{
public:

//========== Rain Effect Tunables ===========
// Various tunable parameters that we will adjust to get the effect we like

// Color of chars to drop
static CRGB dropcolor;
static CRGB trailcolor;

static uint8_t rainfademin;
static uint8_t rainfademax;

// Milliseconds to wait before spawning a new drop, chosen randomly each new drop
static uint minspawnwait;
static uint maxspawnwait;

// Drop speed range in chars per second
static float mindropspeed;
static float maxdropspeed;

static float edgespeed; // in pixels per second (max 60 due to framerate limit)
static int edgedirection; // 0 = inside out, 1 = outside in, 2 = round the pentagon forwards, 3 = round the pentagon backwards
static int edgecolor; // 0 = red and yellow, 1 = hue shifting, 2 = 

static int edgemode; // 0 = stripes crawler, 1 = sparkles
static int starmode;

static float minstarfadein;
static float maxstarfadein;
static float minstarfadeout;
static float maxstarfadeout;

static int minstarlifetime;
static int maxstarlifetime;

static CHSV whitestarcolors[5];

static int glitterdensity;
static float glitterfreq;

static int stardensity;
static float starfreq;

static int cometdensity;
static float cometfreq;
static int cometthrottle;

//=============== Other Category Tunables =============



	static void WriteTunablesToEeprom();

	static void ReadTunablesFromEeprom();

	static void ClearEeprom();

	static void PrintTunables();

	static void PrintHelp();

	// If we stick with the monolithic project structure (all inline .h files) we can just init these values
	// in the declarations above and get rid of this
	static void SetTunableDefaults();

	static bool HandleMessage(const char* message);

};


#endif