
#include "Effect.h"


	void Effect::Init()
	{
		// Nothing, need to subclass this to have something happen!
	}

	void Effect::Update(unsigned long deltamics)
	{
		// Nothing, need to subclass this to have something happen!
	}

	void Effect::Render()
	{
		// Nothing, need to subclass this to have something happen!		
	}
