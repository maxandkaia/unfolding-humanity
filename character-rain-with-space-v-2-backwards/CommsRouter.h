#ifndef COMMS_ROUTER
#define COMMS_ROUTER
#include <arduino.h>
#ifdef sprintf_P
#undef sprintf_P
#endif
#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>
//#include "MessageQueue.h"
#include "MessageHandler.h"

// Change this to suit the radio frequency environment if problems arise
#define RADIO_CHANNEL 121

class CommsRouter
{
public:
	
	// This is the controller that is using us. When we receive messages on the wire, we push them to this controller message queue
	MessageHandler* handler;
	uint16_t this_address;


	RF24* radio;                    // nRF24L01(+) radio attached to pins 9,10 and SPI 
	RF24Network* network;          // Network uses that radio



	CommsRouter(uint16_t network_address, MessageHandler* _handler);

// Moved the controller pointer and net address init to here so we can construct both controller and router without a problem
	void Initialize();
	
	void Reset();

	void Update(unsigned long deltamics);

	// Send a message to a specific controller, use properly null terminated string as the payload!
	bool SendMessageToController(uint16_t destination, const char* message);

	bool SendMessageToControllerNoAck(uint16_t destination, const char* message);

	bool SendMessageToAllPanels(const char* message);

	bool MulticastToAllPanels(const char* message);

	bool SendMulticast(const char* message);

	bool SendMessageToMaster(const char* message);

	bool SendMessageToAllTopEdges(const char* message);

};







#endif