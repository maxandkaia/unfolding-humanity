#ifndef STARS
#define STARS

// The raindrops that we will be managing
#include <arduino.h>
#include "Effect.h"
#include "constants.h"
#include "tunables.h"
#include "timer.h"
#include "number_utils.h"
#include "star.h"

#define NUMMODES 8
#define NUMSTARS 200



// Raineffect owns some "lanes" which correspond to the strands. It spawns raindrops in random lanes. It knows which lanes have active raindrops and always chooses an empty lane in which to spawn
class StarEffect : Effect
{

public:
	Timer glitterTimer;
	Timer starTimer; // how frequently a new star is born to twinkle
	Timer flashTimer; // how frequently a new flashbulb goes off
	Timer cometTimer;

	Star stars[NUMSTARS];

	int mode;

	static CHSV starcolors[5];


	// This Init routine takes an argument of "which segment to control"

	int FindStar();

	void ResetAllStars();

	void ClearAllEdges();

	void Init();

	void Update(unsigned long deltamics);

	void Render();
};


#endif