
/// Includes
#define DEBUG
#include "leds.h"
#include "constants.h"
#include "tunables.h"
#include "debug_utils.h"
#include "raindrop.h"

// Light up a character on the board in the given position with a given color
void raindrop::LightCharacter(uint8_t stripnum, int charposition, CRGB color)
{
		// Some error checking
		if (stripnum < 0 || stripnum > 7)
		{
			DPRINT("bad stripnum in LightCharacter: ");
			return;
		}

		if (charposition < 0 || charposition > 19)
		{
			DPRINT("bad charposition in LightCharacter: "); DPRINT(charposition);
			return;
		}

		uint8_t start = (charposition*6); // Get the first led of the character

		for (int i=0; i<5; i++)
		{
			SetPixel(stripnum,start+i,color);
		}
}



//==========================


void raindrop::Launch(uint8_t _stripnum, int  _position, float _speed, CRGB _color, CRGB _trailcolor, bool _reverse)
{
	// <<TODO>> Validate arguments?
	stripnum = _stripnum;
	position = _position;
	speed = _speed;
	color = _color;
	trailcolor = _trailcolor;
	reverse = _reverse;
	shiftTimer.SetFrequency(speed);
	shiftTimer.Start();
	active = true;
	LightCharacter(stripnum, position, color);
	if (reverse)
		LightCharacter(stripnum, position-1, trailcolor);
	else
		LightCharacter(stripnum, position+1, trailcolor);

	//Serial.println("launching drop...");
}

void raindrop::Update(unsigned long deltamics)
{
	shiftTimer.Update(deltamics);
	if (!active) return;
	if (shiftTimer.GetEvent())
	{
		//Serial.println("moving drop down");
		// Time to move down!
		MoveCharacterDown();
		if (!active) return;

		LightCharacter(stripnum, position, color);
		if (reverse)
			LightCharacter(stripnum, position-1, trailcolor);
		else
			LightCharacter(stripnum, position+1, trailcolor);
	}

}

void raindrop::MoveCharacterDown()
{
	if (reverse)
		position++;
	else
		position--;

	if (position < -1 || position > 20)
	{
		// What to do when the character reaches the end? 
		// Make the strip available again?
		active = false;
		//Serial.println("drop inactive");
	}
}