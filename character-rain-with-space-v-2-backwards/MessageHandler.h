#ifndef MESSAGEHANDLER
#define MESSAGEHANDLER


// abstract class for message handling
class MessageHandler
{
	public:
	virtual void HandleMessage(const char* message, bool fromSerial) = 0;
};

#endif