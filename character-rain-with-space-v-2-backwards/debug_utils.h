#ifndef DEBUG_UTILS
#define DEBUG_UTILS
#include <arduino.h>
//#include "print.h"

#ifdef DEBUG
#define DPRINTF(f_, ...) printf((f_), __VA_ARGS__)
#define DPRINT(a) Serial.println(a)
#else
#define DPRINTF(f_, ...)
#define DPRINT(a)
#endif

#endif